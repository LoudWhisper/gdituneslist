//
//  GDCoreDataManagerTests.m
//  ItunesListTests
//
//  Created by Daniil Gavrilov on 16.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "GDCoreDataManager.h"
#import "GDListModel.h"
#import "GDListModelKeys.h"
#import "GDListTypes.h"

@interface GDCoreDataManagerTests : XCTestCase

@property (nonatomic, strong) GDCoreDataManager *coreDataManager;

@end

@implementation GDCoreDataManagerTests

#pragma mark -
#pragma mark - Standart Methods

- (void)setUp {
    [super setUp];
    
    self.coreDataManager = [[GDCoreDataManager alloc] initWithMaximumCachedRequestsCount:NSIntegerMax];
}

- (void)tearDown {
    [super tearDown];
}

#pragma mark -
#pragma mark - Tests

#pragma mark - Crash Tests

- (void)testSaveNilModelsWillNotCrash {
    XCTAssertNoThrow([self.coreDataManager saveRequestWithSearchString:[self searchString] type:GDListTypeMovie results:nil]);
}

- (void)testSaveEmptyModelsWillNotCrash {
    XCTAssertNoThrow([self.coreDataManager saveRequestWithSearchString:[self searchString] type:GDListTypeMovie results:@[]]);
}

- (void)testSaveNilSearchStringWillNotCrash {
    XCTAssertNoThrow([self.coreDataManager saveRequestWithSearchString:nil type:GDListTypeMovie results:[self listModels]]);
}

- (void)testSaveEmptySearchStringWillNotCrash {
    XCTAssertNoThrow([self.coreDataManager saveRequestWithSearchString:@"" type:GDListTypeMovie results:[self listModels]]);
}

- (void)testSaveNilSearchStringAndNilModelsWillNotCrash {
    XCTAssertNoThrow([self.coreDataManager saveRequestWithSearchString:nil type:GDListTypeMovie results:nil]);
}

- (void)testSaveEmptySearchStringAndEmptyModelsWillNotCrash {
    XCTAssertNoThrow([self.coreDataManager saveRequestWithSearchString:@"" type:GDListTypeMovie results:@[]]);
}

#pragma mark - Service Data Correct Tests

- (void)testCachedRequestsLimitIsCorrect {
    XCTestExpectation *expectation = [self expectationWithDescription:@"Database cached request limit timed out."];
    NSTimeInterval timeout = 1.0f;
    
    GDCoreDataManager *limitedCoreDataManager = [[GDCoreDataManager alloc] initWithMaximumCachedRequestsCount:1];
    NSArray *listModels = [self listModels];
    
    [limitedCoreDataManager saveRequestWithSearchString:[self searchString] type:0 results:listModels];
    [limitedCoreDataManager saveRequestWithSearchString:[self anotherSearchString] type:1 results:listModels];
    [limitedCoreDataManager loadCachedResultsWithSearchString:[self searchString] type:0 completion:^(NSArray<GDListModel *> *results) {
        if ([results count] > 0) {
            XCTFail(@"Database cached request limit count is wrong.");
        }
    }];
    [limitedCoreDataManager loadCachedResultsWithSearchString:[self anotherSearchString] type:1 completion:^(NSArray<GDListModel *> *results) {
        [expectation fulfill];
        
        if ([results count] == [listModels count]) {
            for (NSInteger index = 0; index < [results count]; index++) {
                GDListModel *firstModel = results[index];
                GDListModel *secondModel = listModels[index];
                if (![firstModel isEqual:secondModel]) {
                    XCTFail(@"Database cached request limit order is wrong.");
                }
            }
        } else {
            XCTFail(@"Database cached request limit count is wrong.");
        }
    }];
    
    [self waitForExpectationsWithTimeout:timeout handler:^(NSError * _Nullable error) {
        NSLog(@"%@", error);
    }];
}

- (void)testCachedRequestResultCorrect {
    XCTestExpectation *expectation = [self expectationWithDescription:@"Database cached request result timed out."];
    NSTimeInterval timeout = 1.0f;
    
    GDListModel *listModel = [self listModelWithFullFields];
    [self.coreDataManager saveRequestWithSearchString:[self searchString] type:0 results:@[listModel]];
    [self.coreDataManager loadCachedResultsWithSearchString:[self searchString] type:0 completion:^(NSArray<GDListModel *> *results) {
        [expectation fulfill];
        
        GDListModel *cachedModel = [results firstObject];
        if (cachedModel) {
            XCTAssertEqualObjects(listModel.artistName, cachedModel.artistName, @"obj1(%@) not equal to obj2(%@))", listModel.artistName, cachedModel.artistName);
            XCTAssertEqualObjects(listModel.trackName, cachedModel.trackName, @"obj1(%@) not equal to obj2(%@))", listModel.trackName, cachedModel.trackName);
            XCTAssertEqualObjects(listModel.trackAddress, cachedModel.trackAddress, @"obj1(%@) not equal to obj2(%@))", listModel.trackAddress, cachedModel.trackAddress);
            XCTAssertEqualObjects(listModel.collectionName, cachedModel.collectionName, @"obj1(%@) not equal to obj2(%@))", listModel.collectionName, cachedModel.collectionName);
            XCTAssertEqualObjects(listModel.artworkAddress, cachedModel.artworkAddress, @"obj1(%@) not equal to obj2(%@))", listModel.artworkAddress, cachedModel.artworkAddress);
            XCTAssertEqualObjects(listModel.previewAddress, cachedModel.previewAddress, @"obj1(%@) not equal to obj2(%@))", listModel.previewAddress, cachedModel.previewAddress);
        } else {
            XCTFail(@"Database cached request result is wrong.");
        }
    }];
    
    [self waitForExpectationsWithTimeout:timeout handler:^(NSError * _Nullable error) {
        NSLog(@"%@", error);
    }];
}

#pragma mark -
#pragma mark - Support Methods

- (NSString *)searchString {
    return @"search_string";
}

- (NSString *)anotherSearchString {
    return @"string_search";
}

- (NSArray<GDListModel *> *)listModels {
    return @[[self listModelDataWithFirstNames],
             [self listModelDataWithSecondNames]];
}

- (GDListModel *)listModelDataWithFirstNames {
    id object = @{gd_list_artist_name : @"first",
                  gd_list_track_name : @"first"};
    return [[GDListModel alloc] initWithDictionary:object];
}

- (GDListModel *)listModelDataWithSecondNames {
    id object = @{gd_list_artist_name : @"second",
                  gd_list_track_name : @"second"};
    return [[GDListModel alloc] initWithDictionary:object];
}

- (GDListModel *)listModelWithFullFields {
    id object = @{gd_list_artist_name : @"artist_name",
                  gd_list_artwork_address : @"https://i.imgur.com/0gDzuyO.jpg",
                  gd_list_track_name : @"track_name",
                  gd_list_track_address : @"https://i.imgur.com/0gDzuyO.jpg",
                  gd_list_collection_name : @"collection_name",
                  gd_list_preview_address : @"https://i.imgur.com/0gDzuyO.jpg"};
    return [[GDListModel alloc] initWithDictionary:object];
}

@end
