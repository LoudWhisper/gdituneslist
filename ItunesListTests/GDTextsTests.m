//
//  GDTextsTests.m
//  ItunesListTests
//
//  Created by Daniil Gavrilov on 15.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "NSString+GDTexts.h"

@interface GDTextsTests : XCTestCase

@end

@implementation GDTextsTests

#pragma mark -
#pragma mark - Standart Methods

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

#pragma mark -
#pragma mark - Tests

#pragma mark - Not Nil Tests

- (void)testTitleListViewControllerIsNotNil {
    NSString *string = [NSString gd_title_list_viewcontroller];
    XCTAssertNotNil(string, @"string:%p", string);
}

- (void)testTitleDetailListViewControllerIsNotNil {
    NSString *string = [NSString gd_title_detail_list_viewcontroller];
    XCTAssertNotNil(string, @"string:%p", string);
}

- (void)testTitleErrorAlertIsNotNil {
    NSString *string = [NSString gd_title_error_alert];
    XCTAssertNotNil(string, @"string:%p", string);
}

- (void)testTitleAlertButtonCloseIsNotNil {
    NSString *string = [NSString gd_title_alert_button_close];
    XCTAssertNotNil(string, @"string:%p", string);
}

- (void)testErrorMessageRequestTimedoutIsNotNil {
    NSString *string = [NSString gd_error_message_request_timedout];
    XCTAssertNotNil(string, @"string:%p", string);
}

- (void)testErrorMessageRequestFailedIsNotNil {
    NSString *string = [NSString gd_error_message_request_failed];
    XCTAssertNotNil(string, @"string:%p", string);
}

- (void)testErrorMessageInvalidLinkIsNotNil {
    NSString *string = [NSString gd_error_message_invalid_link];
    XCTAssertNotNil(string, @"string:%p", string);
}

@end
