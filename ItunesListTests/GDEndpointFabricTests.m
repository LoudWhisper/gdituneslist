//
//  GDEndpointFabricTests.m
//  ItunesListTests
//
//  Created by Daniil Gavrilov on 16.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "GDEndpointFabric.h"
#import "GDiTunesListAddressModel.h"

@interface GDEndpointFabricTests : XCTestCase

@property (nonatomic, strong) GDEndpointFabric *endpointFabric;

@end

@implementation GDEndpointFabricTests

#pragma mark -
#pragma mark - Standart Methods

- (void)setUp {
    [super setUp];
    
    self.endpointFabric = [GDEndpointFabric new];
}

- (void)tearDown {
    [super tearDown];
}

#pragma mark -
#pragma mark - Tests

#pragma mark - Crash Tests

- (void)testEndpointWithoutSchemeWillCrash {
    XCTAssertThrows([self.endpointFabric urlWithScheme:nil host:[self host] path:[self path] queryParameters:[self query]]);
}

- (void)testEndpointWithoutHostWillCrash {
    XCTAssertThrows([self.endpointFabric urlWithScheme:[self scheme] host:nil path:[self path] queryParameters:[self query]]);
}

- (void)testEndpointWithoutSchemeAndHostWillCrash {
    XCTAssertThrows([self.endpointFabric urlWithScheme:nil host:nil path:[self path] queryParameters:[self query]]);
}

- (void)testEndpointWithoutPathWillNotCrash {
    XCTAssertNoThrow([self.endpointFabric urlWithScheme:[self scheme] host:[self host] path:nil queryParameters:[self query]]);
}

- (void)testEndpointWithoutQueryWillNotCrash {
    XCTAssertNoThrow([self.endpointFabric urlWithScheme:[self scheme] host:[self host] path:[self path] queryParameters:nil]);
}

#pragma mark - Not Nil Tests

- (void)testEndpointWithoutPathIsNotNil {
    XCTAssertNotNil([self.endpointFabric urlWithScheme:[self scheme] host:[self host] path:nil queryParameters:[self query]]);
}

- (void)testEndpointWithoutQueryIsNotNil {
    XCTAssertNotNil([self.endpointFabric urlWithScheme:[self scheme] host:[self host] path:[self path] queryParameters:nil]);
}

- (void)testEndpointWithoutPathAndQueryIsNotNil {
    XCTAssertNotNil([self.endpointFabric urlWithScheme:[self scheme] host:[self host] path:nil queryParameters:nil]);
}

- (void)testEndpointWithMusicVideoListModelAddressModelIsNotNil {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:GDListTypeMusicVideo];
    XCTAssertNotNil([self.endpointFabric urlWithScheme:[model scheme] host:[model host] path:[model path] queryParameters:[model query]]);
}

- (void)testEndpointWithMovieListModelAddressModelIsNotNil {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:GDListTypeMovie];
    XCTAssertNotNil([self.endpointFabric urlWithScheme:[model scheme] host:[model host] path:[model path] queryParameters:[model query]]);
}

#pragma mark - Endpoint Data Correct Tests

- (void)testEndpointDataCorrect {
    NSString *scheme = [self scheme];
    NSString *host = [self host];
    NSString *path = [self path];
    NSDictionary *queryParameters = [self query];
    
    NSURL *url = [self.endpointFabric urlWithScheme:scheme host:host path:path queryParameters:queryParameters];
    NSURLComponents *components = [[NSURLComponents alloc] initWithURL:url resolvingAgainstBaseURL:NO];
    NSArray *queryItems = [components queryItems];
    NSArray *queryParametersKeys = [queryParameters allKeys];
    
    XCTAssertEqualObjects(scheme, [components scheme], @"obj1(%@) not equal to obj2(%@))", scheme, [components scheme]);
    XCTAssertEqualObjects(host, [components host], @"obj1(%@) not equal to obj2(%@))", host, [components host]);
    XCTAssertEqualObjects(path, [components path], @"obj1(%@) not equal to obj2(%@))", path, [components path]);
    XCTAssertEqualObjects(@([queryParametersKeys count]), @([queryItems count]), @"obj1(%@) not equal to obj2(%@))", @([queryParametersKeys count]), @([queryItems count]));
    
    for (NSInteger index = 0; index < [queryItems count]; index++) {
        NSURLQueryItem *item = [queryItems objectAtIndex:index];
        id parameterKey = [queryParametersKeys objectAtIndex:index];
        
        XCTAssertEqualObjects(parameterKey, [item name], @"obj1(%@) not equal to obj2(%@))", parameterKey, [item name]);
        XCTAssertEqualObjects([queryParameters objectForKey:parameterKey], [item value], @"obj1(%@) not equal to obj2(%@))", [queryParameters objectForKey:parameterKey], [item value]);
    }
}

- (void)testEndpointWithMusicVideoListModelCorrect {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:GDListTypeMusicVideo];
    
    NSURL *url = [self.endpointFabric urlWithScheme:[model scheme] host:[model host] path:[model path] queryParameters:[model query]];
    NSURLComponents *components = [[NSURLComponents alloc] initWithURL:url resolvingAgainstBaseURL:NO];
    NSArray *queryItems = [components queryItems];
    NSArray *queryParametersKeys = [[model query] allKeys];
    
    XCTAssertEqualObjects([model scheme], [components scheme], @"obj1(%@) not equal to obj2(%@))", [model scheme], [components scheme]);
    XCTAssertEqualObjects([model host], [components host], @"obj1(%@) not equal to obj2(%@))", [model host], [components host]);
    XCTAssertEqualObjects([model path], [components path], @"obj1(%@) not equal to obj2(%@))", [model path], [components path]);
    XCTAssertEqualObjects(@([queryParametersKeys count]), @([queryItems count]), @"obj1(%@) not equal to obj2(%@))", @([queryParametersKeys count]), @([queryItems count]));
    
    for (NSInteger index = 0; index < [queryItems count]; index++) {
        NSURLQueryItem *item = [queryItems objectAtIndex:index];
        id parameterKey = [queryParametersKeys objectAtIndex:index];
        
        XCTAssertEqualObjects(parameterKey, [item name], @"obj1(%@) not equal to obj2(%@))", parameterKey, [item name]);
        XCTAssertEqualObjects([[model query] objectForKey:parameterKey], [item value], @"obj1(%@) not equal to obj2(%@))", [[model query] objectForKey:parameterKey], [item value]);
    }
}

- (void)testEndpointWithMovieListModelCorrect {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:GDListTypeMovie];
    
    NSURL *url = [self.endpointFabric urlWithScheme:[model scheme] host:[model host] path:[model path] queryParameters:[model query]];
    NSURLComponents *components = [[NSURLComponents alloc] initWithURL:url resolvingAgainstBaseURL:NO];
    NSArray *queryItems = [components queryItems];
    NSArray *queryParametersKeys = [[model query] allKeys];
    
    XCTAssertEqualObjects([model scheme], [components scheme], @"obj1(%@) not equal to obj2(%@))", [model scheme], [components scheme]);
    XCTAssertEqualObjects([model host], [components host], @"obj1(%@) not equal to obj2(%@))", [model host], [components host]);
    XCTAssertEqualObjects([model path], [components path], @"obj1(%@) not equal to obj2(%@))", [model path], [components path]);
    XCTAssertEqualObjects(@([queryParametersKeys count]), @([queryItems count]), @"obj1(%@) not equal to obj2(%@))", @([queryParametersKeys count]), @([queryItems count]));
    
    for (NSInteger index = 0; index < [queryItems count]; index++) {
        NSURLQueryItem *item = [queryItems objectAtIndex:index];
        id parameterKey = [queryParametersKeys objectAtIndex:index];
        
        XCTAssertEqualObjects(parameterKey, [item name], @"obj1(%@) not equal to obj2(%@))", parameterKey, [item name]);
        XCTAssertEqualObjects([[model query] objectForKey:parameterKey], [item value], @"obj1(%@) not equal to obj2(%@))", [[model query] objectForKey:parameterKey], [item value]);
    }
}

#pragma mark -
#pragma mark - Support Methods

- (NSString *)scheme {
    return @"https";
}

- (NSString *)host {
    return @"server.com";
}

- (NSString *)path {
    return @"/path";
}

- (NSDictionary *)query {
    return @{@"key" : @"value"};
}

@end
