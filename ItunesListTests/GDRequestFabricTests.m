//
//  GDRequestFabricTests.m
//  ItunesListTests
//
//  Created by Daniil Gavrilov on 15.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "GDRequestFabric.h"
#import "GDiTunesListAddressModel.h"

@interface GDRequestFabricTests : XCTestCase

@property (nonatomic, strong) GDRequestFabric *requestFabric;

@end

@implementation GDRequestFabricTests

#pragma mark -
#pragma mark - Standart Methods

- (void)setUp {
    [super setUp];
    
    self.requestFabric = [GDRequestFabric new];
}

- (void)tearDown {
    [super tearDown];
}

#pragma mark -
#pragma mark - Tests

#pragma mark - Crash Tests

- (void)testRequestWithoutSchemeWillCrash {
    XCTAssertThrows([self.requestFabric requestWithScheme:nil host:[self host] path:[self path] queryParameters:[self query]]);
}

- (void)testRequestWithoutHostWillCrash {
    XCTAssertThrows([self.requestFabric requestWithScheme:[self scheme] host:nil path:[self path] queryParameters:[self query]]);
}

- (void)testRequestWithoutSchemeAndHostWillCrash {
    XCTAssertThrows([self.requestFabric requestWithScheme:nil host:nil path:[self path] queryParameters:[self query]]);
}

- (void)testRequestWithoutPathWillNotCrash {
    XCTAssertNoThrow([self.requestFabric requestWithScheme:[self scheme] host:[self host] path:nil queryParameters:[self query]]);
}

- (void)testRequestWithoutQueryWillNotCrash {
    XCTAssertNoThrow([self.requestFabric requestWithScheme:[self scheme] host:[self host] path:[self path] queryParameters:nil]);
}

#pragma mark - Not Nil Tests

- (void)testRequestWithoutPathIsNotNil {
    XCTAssertNotNil([self.requestFabric requestWithScheme:[self scheme] host:[self host] path:nil queryParameters:[self query]]);
}

- (void)testRequestWithoutQueryIsNotNil {
    XCTAssertNotNil([self.requestFabric requestWithScheme:[self scheme] host:[self host] path:[self path] queryParameters:nil]);
}

- (void)testRequestWithoutPathAndQueryIsNotNil {
    XCTAssertNotNil([self.requestFabric requestWithScheme:[self scheme] host:[self host] path:nil queryParameters:nil]);
}

- (void)testRequestWithMusicVideoListModelAddressModelIsNotNil {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:GDListTypeMusicVideo];
    XCTAssertNotNil([self.requestFabric requestWithScheme:[model scheme] host:[model host] path:[model path] queryParameters:[model query]]);
}

- (void)testRequestWithMovieListModelAddressModelIsNotNil {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:GDListTypeMovie];
    XCTAssertNotNil([self.requestFabric requestWithScheme:[model scheme] host:[model host] path:[model path] queryParameters:[model query]]);
}

#pragma mark -
#pragma mark - Support Methods

- (NSString *)scheme {
    return @"https";
}

- (NSString *)host {
    return @"server.com";
}

- (NSString *)path {
    return @"/path";
}

- (NSDictionary *)query {
    return @{@"key" : @"value"};
}

@end
