//
//  GDListModelTests.m
//  ItunesListTests
//
//  Created by Daniil Gavrilov on 15.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "GDListModel.h"
#import "GDListModelKeys.h"

@interface GDListModelTests : XCTestCase

@end

@implementation GDListModelTests

#pragma mark -
#pragma mark - Standart Methods

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

#pragma mark -
#pragma mark - Tests

#pragma mark - Crash Tests

- (void)testInitializationWithNilNotCrash {
    id object = nil;
    XCTAssertNoThrow([[GDListModel alloc] initWithDictionary:object]);
}

- (void)testInitializationWithNullNotCrash {
    id object = [NSNull null];
    XCTAssertNoThrow([[GDListModel alloc] initWithDictionary:object]);
}

#pragma mark - Nil Tests

- (void)testInitializationWithNilIsNil {
    id object = nil;
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertNil(model, @"model:%p", model);
}

- (void)testInitializationWithArrayIsNil {
    id object = @[];
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertNil(model, @"model:%p", model);
}

- (void)testInitializationWithStringIsNil {
    id object = @"";
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertNil(model, @"model:%p", model);
}

- (void)testInitializationWithNumberIsNil {
    id object = @(0);
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertNil(model, @"model:%p", model);
}

- (void)testInitializationWithNullIsNil {
    id object = [NSNull null];
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertNil(model, @"model:%p", model);
}

- (void)testModelInvalidArtworkAddressIsNil {
    id object = [self invalidLinksMaximumListData];
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertNil(model.artworkAddress, @"model.artworkAddress:%p", model.artworkAddress);
}

- (void)testModelInvalidTrackAddressIsNil {
    id object = [self invalidLinksMaximumListData];
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertNil(model.trackAddress, @"model.trackAddress:%p", model.trackAddress);
}

- (void)testModelInvalidPreviewAddressIsNil {
    id object = [self invalidLinksMaximumListData];
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertNil(model.previewAddress, @"model.previewAddress:%p", model.previewAddress);
}

#pragma mark - Not Nil Tests

- (void)testInitializationWithMinimumDataIsNotNil {
    id object = [self correctMinimumListData];
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertNotNil(model, @"model:%p", model);
}

- (void)testInitializationWithMaximumDataIsNotNil {
    id object = [self correctMaximumListData];
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertNotNil(model, @"model:%p", model);
}

#pragma mark - Model Data Correct Tests

- (void)testModelArtistNameCorrect {
    id object = [self correctMaximumListData];
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertEqualObjects(object[gd_list_artist_name], model.artistName, @"obj1(%@) not equal to obj2(%@))", object[gd_list_artist_name], model.artistName);
}

- (void)testModelArtworkAddressCorrect {
    id object = [self correctMaximumListData];
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertEqualObjects(object[gd_list_artwork_address], model.artworkAddress, @"obj1(%@) not equal to obj2(%@))", object[gd_list_artwork_address], model.artworkAddress);
}

- (void)testModelTrackNameCorrect {
    id object = [self correctMaximumListData];
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertEqualObjects(object[gd_list_track_name], model.trackName, @"obj1(%@) not equal to obj2(%@))", object[gd_list_track_name], model.trackName);
}

- (void)testModelTrackAddressCorrect {
    id object = [self correctMaximumListData];
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertEqualObjects(object[gd_list_track_address], model.trackAddress, @"obj1(%@) not equal to obj2(%@))", object[gd_list_track_address], model.trackAddress);
}

- (void)testModelCollectionNameCorrect {
    id object = [self correctMaximumListData];
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertEqualObjects(object[gd_list_collection_name], model.collectionName, @"obj1(%@) not equal to obj2(%@))", object[gd_list_collection_name], model.collectionName);
}

- (void)testModelPreviewAddressCorrect {
    id object = [self correctMaximumListData];
    GDListModel *model = [[GDListModel alloc] initWithDictionary:object];
    XCTAssertEqualObjects(object[gd_list_preview_address], model.previewAddress, @"obj1(%@) not equal to obj2(%@))", object[gd_list_preview_address], model.previewAddress);
}

#pragma mark -
#pragma mark - Support Methods

- (NSDictionary *)correctMaximumListData {
    id object = @{gd_list_artist_name : @"artist_name",
                  gd_list_artwork_address : @"https://i.imgur.com/0gDzuyO.jpg",
                  gd_list_track_name : @"track_name",
                  gd_list_track_address : @"https://i.imgur.com/0gDzuyO.jpg",
                  gd_list_collection_name : @"collection_name",
                  gd_list_preview_address : @"https://i.imgur.com/0gDzuyO.jpg"};
    return object;
}

- (NSDictionary *)correctMinimumListData {
    id object = @{gd_list_artist_name : @"artist_name",
                  gd_list_track_name : @"track_name"};
    return object;
}

- (NSDictionary *)invalidLinksMaximumListData {
    id object = @{gd_list_artist_name : @"artist_name",
                  gd_list_artwork_address : @"not_a_actually_link",
                  gd_list_track_name : @"track_name",
                  gd_list_track_address : @"not_a_actually_link",
                  gd_list_collection_name : @"collection_name",
                  gd_list_preview_address : @"not_a_actually_link"};
    return object;
}

@end
