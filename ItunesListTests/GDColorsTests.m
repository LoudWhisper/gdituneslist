//
//  GDColorsTests.m
//  ItunesListTests
//
//  Created by Daniil on 26.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "UIColor+GDColors.h"

@interface GDColorsTests : XCTestCase

@end

@implementation GDColorsTests

#pragma mark -
#pragma mark - Standart Methods

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

#pragma mark -
#pragma mark - Tests

#pragma mark - Not Nil Tests

- (void)testTitleListViewControllerIsNotNil {
    UIColor *color = [UIColor gd_light_color];
    XCTAssertNotNil(color, @"color:%p", color);
}

- (void)testTitleDetailListViewControllerIsNotNil {
    UIColor *color = [UIColor gd_dark_color];
    XCTAssertNotNil(color, @"color:%p", color);
}

@end
