//
//  GDImagesTests.m
//  ItunesListTests
//
//  Created by Daniil Gavrilov on 15.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "UIImage+GDImages.h"

@interface GDImagesTests : XCTestCase

@end

@implementation GDImagesTests

#pragma mark -
#pragma mark - Standart Methods

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

#pragma mark -
#pragma mark - Tests

#pragma mark - Not Nil Tests

- (void)testArtworkPlaceholderIsNotNil {
    UIImage *image = [UIImage gd_artwork_placeholder];
    XCTAssertNotNil(image, @"image:%p", image);
}

@end

