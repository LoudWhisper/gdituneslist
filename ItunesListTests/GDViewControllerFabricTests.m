//
//  GDViewControllerFabricTests.m
//  ItunesListTests
//
//  Created by Daniil Gavrilov on 16.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "GDViewControllerFabric.h"

#import "GDLaunchViewController.h"
#import "GDListViewController.h"
#import "GDDetailListViewController.h"

@interface GDViewControllerFabricTests : XCTestCase

@property (nonatomic, strong) GDViewControllerFabric *viewControllerFabric;

@end

@implementation GDViewControllerFabricTests

#pragma mark -
#pragma mark - Standart Methods

- (void)setUp {
    [super setUp];
    
    self.viewControllerFabric = [GDViewControllerFabric new];
}

- (void)tearDown {
    [super tearDown];
}

#pragma mark -
#pragma mark - Tests

#pragma mark - Crash Tests

- (void)testViewControllerWithInvalidStoryboardWillCrash {
    NSString *viewControllerIdentifier = NSStringFromClass([GDLaunchViewController class]);
    XCTAssertThrows([self.viewControllerFabric createViewControllerWithIdentifier:viewControllerIdentifier fromStoryboardWithType:NSNotFound]);
}

- (void)testViewControllerWithInvalidViewControllerStoryboardLaunchWillCrash {
    NSString *viewControllerIdentifier = @"";
    XCTAssertThrows([self.viewControllerFabric createViewControllerWithIdentifier:viewControllerIdentifier fromStoryboardWithType:GDStoryboardTypeLaunch]);
}

- (void)testViewControllerWithInvalidViewControllerStoryboardListWillCrash {
    NSString *viewControllerIdentifier = @"";
    XCTAssertThrows([self.viewControllerFabric createViewControllerWithIdentifier:viewControllerIdentifier fromStoryboardWithType:GDStoryboardTypeList]);
}

- (void)testLaunchViewControllerInLaunchStoryboardWillNotCrash {
    NSString *viewControllerIdentifier = NSStringFromClass([GDLaunchViewController class]);
    XCTAssertNoThrow([self.viewControllerFabric createViewControllerWithIdentifier:viewControllerIdentifier fromStoryboardWithType:GDStoryboardTypeLaunch]);
}

- (void)testListViewControllerInListStoryboardWillNotCrash {
    NSString *viewControllerIdentifier = NSStringFromClass([GDListViewController class]);
    XCTAssertNoThrow([self.viewControllerFabric createViewControllerWithIdentifier:viewControllerIdentifier fromStoryboardWithType:GDStoryboardTypeList]);
}

- (void)testDetailListViewControllerInListStoryboardWillNotCrash {
    NSString *viewControllerIdentifier = NSStringFromClass([GDDetailListViewController class]);
    XCTAssertNoThrow([self.viewControllerFabric createViewControllerWithIdentifier:viewControllerIdentifier fromStoryboardWithType:GDStoryboardTypeList]);
}

@end
