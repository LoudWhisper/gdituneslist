//
//  GDExceptionFabricTests.m
//  ItunesListTests
//
//  Created by Daniil Gavrilov on 16.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "GDExceptionFabric.h"

@interface GDExceptionFabricTests : XCTestCase

@property (nonatomic, strong) GDExceptionFabric *exceptionFabric;

@end

@implementation GDExceptionFabricTests

#pragma mark -
#pragma mark - Standart Methods

- (void)setUp {
    [super setUp];
    
    self.exceptionFabric = [GDExceptionFabric new];
}

- (void)tearDown {
    [super tearDown];
}

#pragma mark -
#pragma mark - Tests

#pragma mark - Crash Tests

- (void)testExceptionForViewControllerWithNilWillNotCrash {
    XCTAssertNoThrow([self.exceptionFabric exceptionForViewController:nil viewControllerIdentifier:nil storyboard:nil storyboardName:nil]);
}

- (void)testExceptionForQueryWithNilWillNotCrash {
    XCTAssertNoThrow([self.exceptionFabric exceptionForQueryName:nil queryValue:nil]);
}

- (void)testExceptionForSchemeWithNilWillNotCrash {
    XCTAssertNoThrow([self.exceptionFabric exceptionForScheme:nil host:nil path:nil queryParameters:nil]);
}

- (void)testExceptionForCoreDataModelWithNilWillNotCrash {
    XCTAssertNoThrow([self.exceptionFabric exceptionForCoreDataModelName:nil]);
}

- (void)testExceptionForCoreDataEntityWithNilWillNotCrash {
    XCTAssertNoThrow([self.exceptionFabric exceptionForCoreDataModelName:nil entityName:nil entity:nil]);
}

@end
