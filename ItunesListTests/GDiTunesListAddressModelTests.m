//
//  GDiTunesListAddressModelTests.m
//  ItunesListTests
//
//  Created by Daniil Gavrilov on 15.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "GDiTunesListAddressModel.h"
#import "GDListTypes.h"

@interface GDiTunesListAddressModelTests : XCTestCase

@end

@implementation GDiTunesListAddressModelTests

#pragma mark -
#pragma mark - Standart Methods

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

#pragma mark -
#pragma mark - Tests

#pragma mark - Crash Tests

- (void)testInitializationWithMusicVideoNotCrash {
    XCTAssertNoThrow([[GDiTunesListAddressModel alloc] initWithType:GDListTypeMusicVideo]);
}

- (void)testInitializationWithMovieNotCrash {
    XCTAssertNoThrow([[GDiTunesListAddressModel alloc] initWithType:GDListTypeMovie]);
}

- (void)testInitializationWithMinimumNumberNotCrash {
    XCTAssertNoThrow([[GDiTunesListAddressModel alloc] initWithType:NSIntegerMin]);
}

- (void)testInitializationWithMaximumNumberNotCrash {
    XCTAssertNoThrow([[GDiTunesListAddressModel alloc] initWithType:NSIntegerMax]);
}

- (void)testInitializationWithNotFoundNumberNotCrash {
    XCTAssertNoThrow([[GDiTunesListAddressModel alloc] initWithType:NSNotFound]);
}

#pragma mark - Nil Tests

- (void)testInitializationWithMinimumNumberQueryIsNil {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:NSIntegerMin];
    XCTAssertNil(model.query, @"model.query:%p", model.query);
}

- (void)testInitializationWithMaximumNumberQueryIsNil {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:NSIntegerMax];
    XCTAssertNil(model.query, @"model.query:%p", model.query);
}

- (void)testInitializationWithNotFoundNumberQueryIsNil {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:NSNotFound];
    XCTAssertNil(model.query, @"model.query:%p", model.query);
}

#pragma mark - Not Nil Tests

- (void)testInitializationWithMusicVideoIsNotNil {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:GDListTypeMusicVideo];
    XCTAssertNotNil(model, @"model:%p", model);
}

- (void)testInitializationWithMovieIsNotNil {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:GDListTypeMovie];
    XCTAssertNotNil(model, @"model:%p", model);
}

- (void)testInitializationWithMinimumNumberIsNotNil {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:NSIntegerMin];
    XCTAssertNotNil(model, @"model:%p", model);
}

- (void)testInitializationWithMaximumNumberIsNotNil {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:NSIntegerMax];
    XCTAssertNotNil(model, @"model:%p", model);
}

- (void)testInitializationWithNotFoundNumberIsNotNil {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:NSNotFound];
    XCTAssertNotNil(model, @"model:%p", model);
}

#pragma mark - Model Data Correct Tests

- (void)testInitializationWithMusicVideoCorrect {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:GDListTypeMusicVideo];
    XCTAssertNotNil([model scheme], @"model.scheme:%p", [model scheme]);
    XCTAssertNotNil([model host], @"model.host:%p", [model host]);
    XCTAssertNotNil([model path], @"model.path:%p", [model path]);
    XCTAssertNotNil([model query], @"model.query:%p", [model query]);
}

- (void)testInitializationWithMovieCorrect {
    GDiTunesListAddressModel *model = [[GDiTunesListAddressModel alloc] initWithType:GDListTypeMusicVideo];
    XCTAssertNotNil([model scheme], @"model.scheme:%p", [model scheme]);
    XCTAssertNotNil([model host], @"model.host:%p", [model host]);
    XCTAssertNotNil([model path], @"model.path:%p", [model path]);
    XCTAssertNotNil([model query], @"model.query:%p", [model query]);
}

@end
