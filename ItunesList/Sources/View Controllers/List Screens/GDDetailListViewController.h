//
//  GDDetailListViewController.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 15.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GDDetailListViewModelInputOutput.h"

@protocol GDListScreensProvider;

@interface GDDetailListViewController : UIViewController <GDDetailListViewModelOutput>

@property (nonatomic, weak) id<GDListScreensProvider> router;
@property (nonatomic, strong) id<GDDetailListViewModelInput> viewModel;

@end
