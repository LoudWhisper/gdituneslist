//
//  GDListViewController.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GDListViewModelInputOutput.h"

@protocol GDListScreensProvider;

@interface GDListViewController : UIViewController <GDListViewModelOutput>

@property (nonatomic, weak) id<GDListScreensProvider> router;
@property (nonatomic, strong) id<GDListViewModelInput> viewModel;

@end
