//
//  GDListViewController.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDListViewController.h"

#import "GDListTableViewModel.h"
#import "GDListScreensProvider.h"

#import "GDListLoadingView.h"
#import "GDListPlaceholderView.h"
#import "GDListInstructionView.h"

#import "NSString+GDTexts.h"
#import "UIColor+GDColors.h"

@interface GDListViewController () <UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet GDListTableViewModel *tableViewModel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) UISearchController *searchController;

@property (nonatomic, weak) GDListInstructionView *instructionView;
@property (nonatomic, weak) GDListLoadingView *loadingView;
@property (nonatomic, weak) GDListPlaceholderView *placeholderView;

@end

@implementation GDListViewController

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupSearchController];
    [self setupInstructionView];
    [self.viewModel viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureNavigationBar];
    [self configureTableView];
    [self.viewModel viewWillAppear];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (@available(iOS 11.0, *)) {
        if ([self.tableView contentOffset].y < 0.0) {
            [self.tableView setContentOffset:CGPointMake(0.0, (0.0 - self.view.safeAreaInsets.top)) animated:NO];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[self.searchController searchBar] resignFirstResponder];
    [self.viewModel viewWillDisappear];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - Private Methods

#pragma mark - Setup Methods

- (void)setupSearchController {
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    [self.searchController setHidesNavigationBarDuringPresentation:NO];
    [self.searchController setDimsBackgroundDuringPresentation:NO];
    
    [[self.searchController searchBar] setTintColor:[UIColor gd_dark_color]];
    [[self.searchController searchBar] setDelegate:self];
}

- (void)setupInstructionView {
    GDListInstructionView *instructionView = [[GDListInstructionView alloc] initWithFrame:self.view.bounds];
    [instructionView addAsSubviewTo:self.tableView];
    self.instructionView = instructionView;
}

- (void)configureTableView {
    [self.tableView setKeyboardDismissMode:UIScrollViewKeyboardDismissModeOnDrag];
    
    __weak GDListViewController *wSelf = self;
    [self.tableViewModel setOnSelect:^(GDListModel *model) {
        __strong GDListViewController *sSelf = wSelf;
        [sSelf listItemSelected:model];
    }];
}

- (void)configureNavigationBar {
    self.title = [NSString gd_title_list_viewcontroller];
    
    if (@available(iOS 11.0, *)) {
        [[self.navigationController navigationBar] setPrefersLargeTitles:YES];
        [self.navigationItem setSearchController:self.searchController];
        [self.navigationItem setHidesSearchBarWhenScrolling:NO];
    } else {
        self.navigationItem.titleView = [self.searchController searchBar];
    }
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - State Methods

- (void)applySearchActiveState {
    [[self.searchController searchBar] setShowsCancelButton:YES animated:YES];
    
    [self.placeholderView removeFromSuperview];
    [self.loadingView removeFromSuperview];
    [self.instructionView removeFromSuperview];
    
    GDListLoadingView *loadingView = [[GDListLoadingView alloc] initWithFrame:self.view.bounds];
    [loadingView addAsSubviewTo:self.tableView];
    self.loadingView = loadingView;
}

- (void)applySearchInactiveState {
    [self.loadingView removeFromSuperview];
}

- (void)applySearchCancelledState {
    [self.placeholderView removeFromSuperview];
    [self.loadingView removeFromSuperview];
    [self.instructionView removeFromSuperview];
    
    GDListInstructionView *instructionView = [[GDListInstructionView alloc] initWithFrame:self.view.bounds];
    [instructionView addAsSubviewTo:self.tableView];
    self.instructionView = instructionView;
}

- (void)applySearchCompletedState {
    [self.instructionView removeFromSuperview];
    [self.placeholderView removeFromSuperview];
    
    if ([[self.tableViewModel listModels] count] == 0) {
        GDListPlaceholderView *placeholderView = [[GDListPlaceholderView alloc] initWithFrame:self.view.bounds];
        [placeholderView addAsSubviewTo:self.tableView];
        self.placeholderView = placeholderView;
    }
}

#pragma mark - Support Methods

- (void)listItemSelected:(GDListModel *)listModel {
    [self.router showDetailListViewControllerWithModel:listModel];
}

#pragma mark -
#pragma mark - Protocols Implementation

#pragma mark - GDListViewModelOutput

- (void)listViewModelSearchResultsChanged:(NSArray<GDListModel *> *)results {
    [self.tableViewModel setListModels:results];
    
    if ([self isViewLoaded] && [self.view window]) {
        [self.tableView reloadData];
        [self applySearchCompletedState];
    }
}

- (void)listViewModelDidReceiveErrorWithTitle:(NSString *)title message:(NSString *)message {
    NSString *closeActionTitle = [NSString gd_title_alert_button_close];
    UIAlertAction *closeAction = [UIAlertAction actionWithTitle:closeActionTitle style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:closeAction];
    
    if ([self isViewLoaded] && [self.view window]) {
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)listViewModelSearchStateChanged:(GDListViewModelSearchState)state {
    switch (state) {
        case GDListViewModelSearchStateActive:
            [self applySearchActiveState];
            break;
        case GDListViewModelSearchStateInactive:
            [self applySearchInactiveState];
            break;
        case GDListViewModelSearchStateCancelled:
            [self applySearchCancelledState];
            break;
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.viewModel inputTextChanged:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar setText:nil];
    [searchBar resignFirstResponder];
    
    [self.viewModel cancelSearchButtonClicked];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    
    [self.viewModel startSearchButtonClicked];
}

@end
