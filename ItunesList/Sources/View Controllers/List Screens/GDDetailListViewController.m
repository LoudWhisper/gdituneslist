//
//  GDDetailListViewController.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 15.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDDetailListViewController.h"

#import <AVKit/AVKit.h>

#import "GDListScreensProvider.h"

#import "NSString+GDTexts.h"

@interface GDDetailListViewController ()

@property (nonatomic, weak) IBOutlet UILabel *artistNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *collectionNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *trackNameLabel;
@property (nonatomic, weak) IBOutlet UIButton *openPageButton;
@property (nonatomic, weak) IBOutlet UIView *videoContainerView;

@property (nonatomic, weak) AVPlayerViewController *playerViewController;

@end

@implementation GDDetailListViewController

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.viewModel viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureNavigationBar];
    [self.viewModel viewWillAppear];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UIViewController *viewController = [segue destinationViewController];
    if ([viewController isKindOfClass:[AVPlayerViewController class]]) {
        self.playerViewController = (AVPlayerViewController *)viewController;
        [self.playerViewController setAllowsPictureInPicturePlayback:NO];
        [self.playerViewController setUpdatesNowPlayingInfoCenter:NO];
        [self.playerViewController.view setBackgroundColor:[UIColor clearColor]];
        
        if (@available(iOS 11.0, *)) {
            [self.playerViewController setEntersFullScreenWhenPlaybackBegins:NO];
            [self.playerViewController setExitsFullScreenWhenPlaybackEnds:NO];
        }
    }
}

#pragma mark -
#pragma mark - Private Methods

#pragma mark - Setup Methods

- (void)configureNavigationBar {
    self.title = [NSString gd_title_detail_list_viewcontroller];
    
    if (@available(iOS 11.0, *)) {
        [[self.navigationController navigationBar] setPrefersLargeTitles:YES];
    }
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)setupPreviewViewWithURL:(NSURL *)previewURL {
    AVPlayer *player = [[AVPlayer alloc] initWithURL:previewURL];
    [player setAutomaticallyWaitsToMinimizeStalling:YES];
    
    [self.playerViewController setPlayer:player];
}

#pragma mark - Setup Methods

#pragma mark -
#pragma mark - Controls Actions

- (IBAction)openPageButtonAction:(id)sender {
    [self.viewModel openPageButtonClicked];
}

#pragma mark -
#pragma mark - Protocols Implementation

#pragma mark - GDDetailListViewModelOutput

- (void)detailListViewModelShouldApplyArtistName:(NSString *)artistName collectionName:(NSString *)collectionName trackName:(NSString *)trackName trackURL:(NSURL *)trackURL previewURL:(NSURL *)previewURL {
    [self.artistNameLabel setText:artistName];
    [self.collectionNameLabel setText:collectionName];
    [self.trackNameLabel setText:trackName];
    
    [self.openPageButton setEnabled:(trackURL != nil)];
    
    [self setupPreviewViewWithURL:previewURL];
}

- (void)detailListViewModelShouldOpenWebPageWithURL:(NSURL *)url {
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
}

- (void)detailListViewModelDidReceiveErrorWithTitle:(NSString *)title message:(NSString *)message {
    NSString *closeActionTitle = [NSString gd_title_alert_button_close];
    UIAlertAction *closeAction = [UIAlertAction actionWithTitle:closeActionTitle style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:closeAction];
    
    if ([self isViewLoaded] && [self.view window]) {
        [self presentViewController:alert animated:YES completion:nil];
    }
}

@end
