//
//  GDLaunchViewController.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GDLaunchViewModelInputOutput.h"

@protocol GDLaunchScreensProvider;

@interface GDLaunchViewController : UIViewController <GDLaunchViewModelOutput>

@property (nonatomic, weak) id<GDLaunchScreensProvider> router;
@property (nonatomic, strong) id<GDLaunchViewModelInput> viewModel;

@end
