//
//  GDLaunchViewController.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDLaunchViewController.h"

#import "GDLaunchScreensProvider.h"

@interface GDLaunchViewController ()

@property (nonatomic, weak) IBOutlet UISegmentedControl *listSegmentedControl;

@end

@implementation GDLaunchViewController

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.viewModel viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureNavigationBar];
    [self.viewModel viewWillAppear];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - Private Methods

#pragma mark - Setup Methods

- (void)configureNavigationBar {
    self.title = nil;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark -
#pragma mark - Controls Actions

- (IBAction)showListButtonAction:(id)sender {
    NSInteger selectedSegment = [self.listSegmentedControl selectedSegmentIndex];
    [self.viewModel showListButtonClickedWithSelectedIndex:selectedSegment];
}

#pragma mark -
#pragma mark - Protocols Implementation

#pragma mark - GDLaunchViewModelOutput

- (void)launchViewModelShouldShowMusicVideosList {
    [self.router showMusicVideoList];
}

- (void)launchViewModelShouldShowMoviesList {
    [self.router showMovieList];
}

@end
