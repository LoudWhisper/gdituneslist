//
//  GDListScreensProvider.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GDListTypes.h"

@class GDRouter;
@class GDListModel;

@protocol GDListScreensProvider <NSObject>

@required

/**
 Shows the initial view controller in the current navigation stack.
 */
- (void)showInitialViewController;

/**
 Shows detail information about selected iTunes item.

 @param listModel Selected iTunes item.
 */
- (void)showDetailListViewControllerWithModel:(GDListModel *)listModel;

@end
