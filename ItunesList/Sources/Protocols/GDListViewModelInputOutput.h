//
//  GDListViewModelInputOutput.h
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDViewControllerObserver.h"

@class GDListModel;

typedef NS_ENUM(NSUInteger, GDListViewModelSearchState) {
    GDListViewModelSearchStateActive,
    GDListViewModelSearchStateInactive,
    GDListViewModelSearchStateCancelled
};

@protocol GDListViewModelOutput <NSObject>

@required

/**
 Asks the view controller to show search results.

 @param results Search results.
 */
- (void)listViewModelSearchResultsChanged:(NSArray<GDListModel *> *)results;

/**
 Asks the view controller to show search state.
 
 @param state Search state.
 */
- (void)listViewModelSearchStateChanged:(GDListViewModelSearchState)state;

/**
 Asks the view controller to show error.

 @param title Error title.
 @param message Error message.
 */
- (void)listViewModelDidReceiveErrorWithTitle:(NSString *)title
                                      message:(NSString *)message;

@end



@protocol GDListViewModelInput <GDViewControllerObserver>

@required

/**
 Called when the view controller wants to start the search.
 */
- (void)startSearchButtonClicked;

/**
 Called when the view controller wants to cancel the search.
 */
- (void)cancelSearchButtonClicked;

/**
 Called when the view controller changes search text.

 @param text Search text.
 */
- (void)inputTextChanged:(NSString *)text;

@end
