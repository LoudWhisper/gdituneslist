//
//  GDDetailListViewModelInputOutput.h
//  ItunesList
//
//  Created by Daniil on 15.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDViewControllerObserver.h"

@class GDListModel;

@protocol GDDetailListViewModelOutput <NSObject>

@required

/**
 Asks the view controller to show iTunes item information.

 @param artistName iTunes item artist name.
 @param collectionName iTunes item callection name.
 @param trackName iTunes item track name.
 @param trackURL iTunes item track URL.
 @param previewURL iTunes item preview URL.
 */
- (void)detailListViewModelShouldApplyArtistName:(NSString *)artistName
                                  collectionName:(NSString *)collectionName
                                       trackName:(NSString *)trackName
                                        trackURL:(NSURL *)trackURL
                                      previewURL:(NSURL *)previewURL;

/**
 Asks the view controller to open web page with URL.

 @param url Web page URL.
 */
- (void)detailListViewModelShouldOpenWebPageWithURL:(NSURL *)url;

/**
 Asks the view controller to show error.
 
 @param title Error title.
 @param message Error message.
 */
- (void)detailListViewModelDidReceiveErrorWithTitle:(NSString *)title
                                            message:(NSString *)message;

@end



@protocol GDDetailListViewModelInput <GDViewControllerObserver>

@required

/**
 Called when the view controller wants to open iTunes item web page.
 */
- (void)openPageButtonClicked;

@end
