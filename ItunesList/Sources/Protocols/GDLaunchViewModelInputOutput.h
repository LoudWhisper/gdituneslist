//
//  GDLaunchViewModelInputOutput.h
//  ItunesList
//
//  Created by Daniil on 15.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDViewControllerObserver.h"

@protocol GDLaunchViewModelOutput <NSObject>

@required

/**
 Asks the view controller to open the list of music videos.
 */
- (void)launchViewModelShouldShowMusicVideosList;

/**
 Asks the view controller to open the list of movies.
 */
- (void)launchViewModelShouldShowMoviesList;

@end



@protocol GDLaunchViewModelInput <GDViewControllerObserver>

@required

/**
 Called when the view controller selects a list index.

 @param selectedIndex Selected index.
 */
- (void)showListButtonClickedWithSelectedIndex:(NSInteger)selectedIndex;

@end
