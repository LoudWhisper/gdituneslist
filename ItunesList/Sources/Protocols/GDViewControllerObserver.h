//
//  GDViewControllerObserver.h
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GDViewControllerObserver <NSObject>

@required

/**
 Called on view controller's 'viewDidLoad:'.
 */
- (void)viewDidLoad;

/**
 Called on view controller's 'viewWillAppear:'.
 */
- (void)viewWillAppear;

/**
 Called on view controller's 'viewDidAppear:'.
 */
- (void)viewDidAppear;

/**
 Called on view controller's 'viewWillDisappear:'.
 */
- (void)viewWillDisappear;

/**
 Called on view controller's 'viewDidDisappear:'.
 */
- (void)viewDidDisappear;

@end
