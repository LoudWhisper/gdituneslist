//
//  LaunchScreensProvider.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GDLaunchScreensProvider <NSObject>

@required

/**
 Shows iTunes list with music videos.
 */
- (void)showMusicVideoList;

/**
 Shows iTunes list with movies.
 */
- (void)showMovieList;

@end
