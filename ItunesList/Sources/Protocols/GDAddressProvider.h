//
//  GDAddressProvider.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GDAddressProvider <NSObject>

@required

/**
 @return Address scheme. E.g. 'https'.
 */
- (NSString *)scheme;

/**
 @return Address host. E.g. 'itunes.apple.com'.
 */
- (NSString *)host;

/**
 @return Address path. E.g. '/search'.
 */
- (NSString *)path;

/**
 @return Address query parameters.
 */
- (NSDictionary *)query;

@end
