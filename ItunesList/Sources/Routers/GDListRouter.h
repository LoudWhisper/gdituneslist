//
//  GDListRouter.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 13.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDRouter.h"
#import "GDListScreensProvider.h"

@interface GDListRouter : GDRouter <GDListScreensProvider>

/**
 Initialize with the parent router. Don't forget to set a child router for the parent router.
 
 @param parentRouter Parent router.
 @param type iTunes list type.
 @return Instance of GDRouter.
 */
- (instancetype)initWithParentRouter:(GDRouter *)parentRouter
                                type:(GDListType)type NS_DESIGNATED_INITIALIZER;

- (instancetype)initWithParentRouter:(GDRouter *)parentRouter __attribute__((unavailable("'initWithParentRouter:' not available, use 'initWithParentRouter:type:' instead.")));

@end
