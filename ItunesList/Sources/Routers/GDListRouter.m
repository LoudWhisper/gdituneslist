//
//  GDListRouter.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 13.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDListRouter.h"
#import "GDRouter+PropertiesContainer.h"

#import "GDListViewController.h"
#import "GDListViewModel.h"
#import "GDListDataModel.h"

#import "GDDetailListViewController.h"
#import "GDDetailListViewModel.h"

#import "GDAddressesProvider.h"
#import "GDListProvider.h"
#import "GDStorageProvider.h"

#import "GDViewControllerFabric.h"

@interface GDListRouter ()

@property (nonatomic) GDListType type;
@property (nonatomic, strong) GDAddressesProvider *addressesProvider;

@end

@implementation GDListRouter

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithParentRouter:(GDRouter *)parentRouter type:(GDListType)type {
    self = [super initWithParentRouter:parentRouter];
    if (self) {
        self.type = type;
        self.addressesProvider = [GDAddressesProvider new];
    }
    return self;
}

#pragma mark -
#pragma mark - Public Methods

- (void)showInitialViewController {
    UIViewController *viewController = [self setupListViewController];
    [self setViewControllers: @[viewController] withFadeAnimationToNavigationController:self.rootNavigationController];
}

- (void)showDetailListViewControllerWithModel:(GDListModel *)listModel {
    UIViewController *viewController = [self setupDetailListViewControllerWithModel:listModel];
    [self.rootNavigationController pushViewController:viewController animated:YES];
}

#pragma mark -
#pragma mark - Private Methods

#pragma mark - Setup Methods

- (UIViewController *)setupListViewController {
    NSString *viewControllerIdentifier = NSStringFromClass([GDListViewController class]);
    GDStoryboardType storyboardType = GDStoryboardTypeList;
    id<GDAddressProvider> addressProvider = [self addressProviderForType:self.type];
    
    GDListProvider *listProvider = [[GDListProvider alloc] initWithScheme:[addressProvider scheme] host:[addressProvider host] path:[addressProvider path] queryParameters:[addressProvider query]];
    GDStorageProvider *storageProvider = [[GDStorageProvider alloc] initWithListType:self.type];
    GDListDataModel *listDataModel = [[GDListDataModel alloc] initWithListProvider:listProvider storageProvider:storageProvider];
    GDListViewController *listViewController = [self.viewControllerFabric createViewControllerWithIdentifier:viewControllerIdentifier fromStoryboardWithType:storyboardType];
    GDListViewModel *listViewModel = [[GDListViewModel alloc] initWithListDataModel:listDataModel output:listViewController];
    
    [listViewController setRouter:self];
    [listViewController setViewModel:listViewModel];
    return listViewController;
}

- (UIViewController *)setupDetailListViewControllerWithModel:(GDListModel *)listModel {
    NSString *viewControllerIdentifier = NSStringFromClass([GDDetailListViewController class]);
    GDStoryboardType storyboardType = GDStoryboardTypeList;
    
    GDDetailListViewController *detailListViewController = [self.viewControllerFabric createViewControllerWithIdentifier:viewControllerIdentifier fromStoryboardWithType:storyboardType];
    GDDetailListViewModel *detailListViewModel = [[GDDetailListViewModel alloc] initWithListModel:listModel output:detailListViewController];
    
    [detailListViewController setRouter:self];
    [detailListViewController setViewModel:detailListViewModel];
    return detailListViewController;
}

#pragma mark - Animations

- (void)setViewControllers:(NSArray *)viewControllers withFadeAnimationToNavigationController:(UINavigationController *)navigationController {
    CAMediaTimingFunction *timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    CATransition *transition = [CATransition new];
    [transition setDuration:0.3];
    [transition setTimingFunction:timingFunction];
    [transition setType:kCATransitionFade];
    
    [CATransaction begin];
    [[[navigationController view] layer] addAnimation:transition forKey:@"setViewControllersWithFadeAnimation"];
    [navigationController setViewControllers:viewControllers];
    [CATransaction commit];
}

#pragma mark - Support Methods

- (id<GDAddressProvider>)addressProviderForType:(GDListType)type {
    switch (type) {
        case GDListTypeMusicVideo:
            return [self.addressesProvider musicVideoListAddress];
        case GDListTypeMovie:
            return [self.addressesProvider movieListAddress];
    }
    
    return nil;
}

@end
