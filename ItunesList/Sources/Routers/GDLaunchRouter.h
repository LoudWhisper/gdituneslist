//
//  GDLaunchRouter.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 13.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDRouter.h"
#import "GDLaunchScreensProvider.h"

@interface GDLaunchRouter : GDRouter <GDLaunchScreensProvider>

/**
 Initialize initial router with the application window.
 
 @param window Application window.
 @return Instance of GDRouter.
 */
- (instancetype)initWithWindow:(UIWindow *)window NS_DESIGNATED_INITIALIZER;

- (instancetype)initWithParentRouter:(GDRouter *)parentRouter __attribute__((unavailable("'initWithParentRouter:' not available, use 'initWithWindow:' instead.")));

@end
