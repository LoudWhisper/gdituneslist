//
//  GDRouter.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GDRouter : NSObject

/**
 Initialize with the parent router. Don't forget to set a child router for the parent router.

 @param parentRouter Parent router.
 @return Instance of GDRouter.
 */
- (instancetype)initWithParentRouter:(GDRouter *)parentRouter NS_DESIGNATED_INITIALIZER;

- (instancetype)init __attribute__((unavailable("'init' not available, use 'initWithParentRouter:' instead.")));

@end
