//
//  GDLaunchRouter.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 13.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDLaunchRouter.h"
#import "GDRouter+PropertiesContainer.h"

#import "GDLaunchViewController.h"
#import "GDLaunchViewModel.h"

#import "GDListRouter.h"

#import "GDViewControllerFabric.h"

@implementation GDLaunchRouter

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithWindow:(UIWindow *)window {
    self = [super initWithParentRouter:nil];
    if (self) {
        self.rootNavigationController = [self setupLaunchNavigationControllerWithWindow:window];
    }
    return self;
}

#pragma mark -
#pragma makr - Public Methods

- (void)showMusicVideoList {
    GDListRouter *listRouter = [[GDListRouter alloc] initWithParentRouter:self type:GDListTypeMusicVideo];
    [listRouter showInitialViewController];
    
    self.childRouter = listRouter;
}

- (void)showMovieList {
    GDListRouter *listRouter = [[GDListRouter alloc] initWithParentRouter:self type:GDListTypeMovie];
    [listRouter showInitialViewController];
    
    self.childRouter = listRouter;
}

#pragma mark -
#pragma mark - Private Methods

#pragma mark - Setup Methods

- (UINavigationController *)setupLaunchNavigationControllerWithWindow:(UIWindow *)window {
    NSString *viewControllerIdentifier = NSStringFromClass([GDLaunchViewController class]);
    GDStoryboardType storyboardType = GDStoryboardTypeLaunch;
    
    GDLaunchViewController *launchViewController = [self.viewControllerFabric createViewControllerWithIdentifier:viewControllerIdentifier fromStoryboardWithType:storyboardType];
    GDLaunchViewModel *launchViewModel = [[GDLaunchViewModel alloc] initWithOutput:launchViewController];
    
    [launchViewController setRouter:self];
    [launchViewController setViewModel:launchViewModel];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:launchViewController];
    [window setRootViewController:navigationController];
    return navigationController;
}

@end
