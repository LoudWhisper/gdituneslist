//
//  GDRouter.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDRouter.h"

#import "GDViewControllerFabric.h"

@interface GDRouter ()

@property (nonatomic, strong) UINavigationController *rootNavigationController;
@property (nonatomic, strong) GDRouter *childRouter;
@property (nonatomic, weak) GDRouter *parentRouter;
@property (nonatomic, strong) GDViewControllerFabric *viewControllerFabric;

@end

@implementation GDRouter

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithParentRouter:(GDRouter *)parentRouter {
    self = [super init];
    if (self) {
        self.parentRouter = parentRouter;
        self.rootNavigationController = [parentRouter rootNavigationController];
        self.viewControllerFabric = [GDViewControllerFabric new];
    }
    return self;
}

@end
