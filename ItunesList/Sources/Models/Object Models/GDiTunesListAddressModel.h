//
//  GDiTunesListAddressModel.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GDAddressProvider.h"

#import "GDListTypes.h"

@interface GDiTunesListAddressModel : NSObject <GDAddressProvider>

/**
 Initialize address model.

 @param type iTunes list type.
 @return Instance of GDiTunesListAddressModel.
 */
- (instancetype)initWithType:(GDListType)type NS_DESIGNATED_INITIALIZER;

- (instancetype)init __attribute__((unavailable("'init' not available, use 'initWithType:' instead.")));

@end
