//
//  GDListModel.m
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDListModel.h"

#import "GDListModelKeys.h"

@interface GDListModel () <NSCoding>

@property (nonatomic, copy, readwrite) NSString *artistName;

@property (nonatomic, copy, readwrite) NSString *trackName;
@property (nonatomic, copy, readwrite) NSString *trackAddress;

@property (nonatomic, copy, readwrite) NSString *collectionName;
@property (nonatomic, copy, readwrite) NSString *artworkAddress;
@property (nonatomic, copy, readwrite) NSString *previewAddress;

@end

@implementation GDListModel

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (dictionary && [dictionary isKindOfClass:[NSDictionary class]]) {
        NSString *artistName = [self stringFromDictionary:dictionary withKey:gd_list_artist_name];
        NSString *artworkAddress = [self addressStringFromDictionary:dictionary withKey:gd_list_artwork_address];
        NSString *trackName = [self stringFromDictionary:dictionary withKey:gd_list_track_name];
        NSString *trackAddress = [self addressStringFromDictionary:dictionary withKey:gd_list_track_address];
        NSString *collectionName = [self stringFromDictionary:dictionary withKey:gd_list_collection_name];
        NSString *previewAddress = [self addressStringFromDictionary:dictionary withKey:gd_list_preview_address];
        
        if (artistName && trackName) {
            self = [super init];
            if (self) {
                self.artistName = artistName;
                
                self.trackName = trackName;
                self.trackAddress = trackAddress;
                
                self.collectionName = collectionName;
                self.artworkAddress = artworkAddress;
                self.previewAddress = previewAddress;
            }
            return self;
        }
    }
    
    return nil;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    NSString *artistName = [aDecoder decodeObjectForKey:gd_list_artist_name];
    NSString *trackName = [aDecoder decodeObjectForKey:gd_list_track_name];
    NSString *trackAddress = [aDecoder decodeObjectForKey:gd_list_track_address];
    NSString *collectionName = [aDecoder decodeObjectForKey:gd_list_collection_name];
    NSString *artworkAddress = [aDecoder decodeObjectForKey:gd_list_artwork_address];
    NSString *previewAddress = [aDecoder decodeObjectForKey:gd_list_preview_address];
    
    NSMutableDictionary *modelInformation = [NSMutableDictionary new];
    if (artistName) {
        [modelInformation setObject:artistName forKey:gd_list_artist_name];
    }
    if (trackName) {
        [modelInformation setObject:trackName forKey:gd_list_track_name];
    }
    if (trackAddress) {
        [modelInformation setObject:trackAddress forKey:gd_list_track_address];
    }
    if (collectionName) {
        [modelInformation setObject:collectionName forKey:gd_list_collection_name];
    }
    if (artworkAddress) {
        [modelInformation setObject:artistName forKey:gd_list_artwork_address];
    }
    if (previewAddress) {
        [modelInformation setObject:previewAddress forKey:gd_list_preview_address];
    }
    
    return [self initWithDictionary:modelInformation];
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.artistName forKey:gd_list_artist_name];
    [aCoder encodeObject:self.trackName forKey:gd_list_track_name];
    
    if (self.trackAddress) {
        [aCoder encodeObject:self.trackAddress forKey:gd_list_track_address];
    }
    if (self.collectionName) {
        [aCoder encodeObject:self.collectionName forKey:gd_list_collection_name];
    }
    if (self.artworkAddress) {
        [aCoder encodeObject:self.artworkAddress forKey:gd_list_artwork_address];
    }
    if (self.previewAddress) {
        [aCoder encodeObject:self.previewAddress forKey:gd_list_preview_address];
    }
}

#pragma mark -
#pragma mark - Private Methods

- (NSString *)stringFromDictionary:(NSDictionary *)dictionary withKey:(NSString *)key {
    if (dictionary && [dictionary isKindOfClass:[NSDictionary class]] && key && [key isKindOfClass:[NSString class]]) {
        id object = [dictionary objectForKey:key];
        if (object && ![object isEqual:[NSNull null]]) {
            NSString *string = [NSString stringWithFormat:@"%@", object];
            return string;
        }
    }
    
    return nil;
}

- (NSString *)addressStringFromDictionary:(NSDictionary *)dictionary withKey:(NSString *)key {
    if (dictionary && [dictionary isKindOfClass:[NSDictionary class]] && key && [key isKindOfClass:[NSString class]]) {
        id object = [dictionary objectForKey:key];
        if (object && ![object isEqual:[NSNull null]]) {
            NSString *string = [NSString stringWithFormat:@"%@", object];
            NSURL *url = [NSURL URLWithString:string];
            if (url && [url scheme] && [url host]) {
                return string;
            }
        }
    }
    
    return nil;
}

@end
