//
//  GDListModel.h
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GDListModel : NSObject

@property (nonatomic, copy, readonly) NSString *artistName;

@property (nonatomic, copy, readonly) NSString *trackName;
@property (nonatomic, copy, readonly) NSString *trackAddress;

@property (nonatomic, copy, readonly) NSString *collectionName;
@property (nonatomic, copy, readonly) NSString *artworkAddress;
@property (nonatomic, copy, readonly) NSString *previewAddress;

/**
 Initialize list model with dictionary values.

 @param dictionary Server dictionary.
 @return Instance of GDListModel.
 */
- (instancetype)initWithDictionary:(NSDictionary *)dictionary NS_DESIGNATED_INITIALIZER;

- (instancetype)init __attribute__((unavailable("'init' not available, use 'initWithDictionary:' instead.")));

@end
