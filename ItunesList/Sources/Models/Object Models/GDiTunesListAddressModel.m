//
//  GDiTunesListAddressModel.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDiTunesListAddressModel.h"

#import "GDListRequestKeysValues.h"

@interface GDiTunesListAddressModel ()

@property (nonatomic, copy) NSString *addressScheme;
@property (nonatomic, copy) NSString *addressHost;
@property (nonatomic, copy) NSString *addressPath;
@property (nonatomic, strong) NSDictionary *addressQuery;

@end

@implementation GDiTunesListAddressModel

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithType:(GDListType)type {
    self = [super init];
    if (self) {
        [self setupWithType:type];
    }
    return self;
}

#pragma mark -
#pragma mark - Public Methods

- (NSString *)scheme {
    return self.addressScheme;
}

- (NSString *)host {
    return self.addressHost;
}

- (NSString *)path {
    return self.addressPath;
}

- (NSDictionary *)query {
    return self.addressQuery;
}

#pragma mark -
#pragma mark - Private Methods

#pragma mark - Setup Methods

- (void)setupWithType:(GDListType)type {
    self.addressScheme = @"https";
    self.addressHost = @"itunes.apple.com";
    self.addressPath = @"/search";
    
    switch (type) {
        case GDListTypeMusicVideo:
            self.addressQuery = @{gd_list_request_search_type_key : gd_list_request_music_video_value};
            break;
        case GDListTypeMovie:
            self.addressQuery = @{gd_list_request_search_type_key : gd_list_request_movie_value};
            break;
    }
}

@end
