//
//  GDListDataModel.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 13.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GDCompletionBlocks.h"

@class GDListProvider;
@class GDStorageProvider;
@class GDListModel;

@interface GDListDataModel : NSObject

/**
 Initialize data model with data providers.

 @param listProvider iTunes list server data provider.
 @param storageProvider iTunes list database data provider.
 @return Instance of GDListDataModel.
 */
- (instancetype)initWithListProvider:(GDListProvider *)listProvider
                     storageProvider:(GDStorageProvider *)storageProvider NS_DESIGNATED_INITIALIZER;

- (instancetype)init __attribute__((unavailable("'init' not available, use 'initWithListProvider:storageProvider:' instead.")));

/**
 Loads cached result models.
 
 @param string Search string.
 @param completion Load completion block.
 */
- (void)loadCachedWithString:(NSString *)string
                  completion:(GDDataProviderCompletionBlock)completion;

/**
 Starts search.
 
 @param string Search string.
 @param completion Search completion block.
 */
- (void)searchWithString:(NSString *)string
              completion:(GDDataProviderCompletionBlock)completion;

/**
 Cancels search.
 */
- (void)cancelSearch;

@end
