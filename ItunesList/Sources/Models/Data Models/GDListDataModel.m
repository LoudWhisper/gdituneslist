//
//  GDListDataModel.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 13.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDListDataModel.h"

#import "GDListProvider.h"
#import "GDStorageProvider.h"

@interface GDListDataModel ()

@property (nonatomic, strong) GDListProvider *listProvider;
@property (nonatomic, strong) GDStorageProvider *storageProvider;

@end

@implementation GDListDataModel

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithListProvider:(GDListProvider *)listProvider storageProvider:(GDStorageProvider *)storageProvider {
    self = [super init];
    if (self) {
        self.listProvider = listProvider;
        self.storageProvider = storageProvider;
    }
    return self;
}

#pragma mark -
#pragma mark - Public Methods

- (void)loadCachedWithString:(NSString *)string completion:(GDDataProviderCompletionBlock)completion {
    if (string && [string length] > 0) {
        NSString *clearString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        __weak GDListDataModel *wSelf = self;
        [self.storageProvider loadCachedResultsWithSearchString:clearString completion:^(NSArray<GDListModel *> *results) {
            __strong GDListDataModel *sSelf = wSelf;
            [sSelf performCompletionOnMainThread:completion withError:nil results:results];
        }];
    } else {
        [self performCompletionOnMainThread:completion withError:nil results:nil];
    }
}

- (void)searchWithString:(NSString *)string completion:(GDDataProviderCompletionBlock)completion {
    if (string && [string length] > 0) {
        NSString *clearString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        __weak GDListDataModel *wSelf = self;
        [self.listProvider searchWithString:clearString completion:^(NSString *error, NSArray<GDListModel *> *results) {
            __strong GDListDataModel *sSelf = wSelf;
            [sSelf searchSucceededWithString:clearString results:results completion:completion];
        }];
    } else {
        [self performCompletionOnMainThread:completion withError:nil results:nil];
    }
}

- (void)cancelSearch {
    [self.listProvider cancelSearch];
}

#pragma mark -
#pragma mark - Private Methods

#pragma mark - Support Methods

- (void)searchSucceededWithString:(NSString *)string results:(NSArray *)results completion:(GDDataProviderCompletionBlock)completion {
    [self.storageProvider saveRequestWithSearchString:string results:results];
    [self performCompletionOnMainThread:completion withError:nil results:results];
}

- (void)performCompletionOnMainThread:(GDDataProviderCompletionBlock)completion withError:(NSString *)error results:(NSArray *)results {
    if (!completion) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        completion(error, results);
    });
}

@end
