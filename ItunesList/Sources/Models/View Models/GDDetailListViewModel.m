//
//  GDDetailListViewModel.m
//  ItunesList
//
//  Created by Daniil on 15.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDDetailListViewModel.h"

#import "GDListModel.h"

#import "NSString+GDTexts.h"

@interface GDDetailListViewModel ()

@property (nonatomic, strong) GDListModel *listModel;
@property (nonatomic, weak) id<GDDetailListViewModelOutput> output;

@end

@implementation GDDetailListViewModel

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithListModel:(GDListModel *)listModel output:(id<GDDetailListViewModelOutput>)output {
    self = [super init];
    if (self) {
        self.listModel = listModel;
        self.output = output;
    }
    return self;
}

#pragma mark -
#pragma mark - Private Methods

- (NSURL *)trackURL {
    NSURL *trackURL;
    NSString *trackAddress = [self.listModel trackAddress];
    if (trackAddress && [trackAddress length] > 0) {
        trackURL = [NSURL URLWithString:trackAddress];
    }
    return trackURL;
}

- (NSURL *)previewURL {
    NSURL *previewURL;
    NSString *previewAddress = [self.listModel previewAddress];
    if (previewAddress && [previewAddress length] > 0) {
        previewURL = [NSURL URLWithString:previewAddress];
    }
    return previewURL;
}

#pragma mark -
#pragma mark - Public Methods

#pragma mark - GDDetailListViewModelInput

- (void)openPageButtonClicked {
    NSURL *trackURL = [self trackURL];
    if (trackURL) {
        [self.output detailListViewModelShouldOpenWebPageWithURL:trackURL];
    } else {
        NSString *errorTitle = [NSString gd_title_error_alert];
        NSString *errorMessage = [NSString gd_error_message_invalid_link];
        [self.output detailListViewModelDidReceiveErrorWithTitle:errorTitle message:errorMessage];
    }
}

#pragma mark - GDViewControllerObserver

- (void)viewDidLoad {
    
}

- (void)viewWillAppear {
    NSString *artistName = [self.listModel artistName];
    NSString *collectionName = [self.listModel collectionName];
    NSString *trackName = [self.listModel trackName];
    
    NSURL *trackURL = [self trackURL];
    NSURL *previewURL = [self previewURL];
    
    [self.output detailListViewModelShouldApplyArtistName:artistName collectionName:collectionName trackName:trackName trackURL:trackURL previewURL:previewURL];
}

- (void)viewDidAppear {
    
}

- (void)viewWillDisappear {
    
}

- (void)viewDidDisappear {
    
}

@end
