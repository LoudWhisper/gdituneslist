//
//  GDLaunchViewModel.m
//  ItunesList
//
//  Created by Daniil on 15.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDLaunchViewModel.h"

#import "GDListTypes.h"

@interface GDLaunchViewModel ()

@property (nonatomic, weak) id<GDLaunchViewModelOutput> output;

@end

@implementation GDLaunchViewModel

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithOutput:(id<GDLaunchViewModelOutput>)output {
    self = [super init];
    if (self) {
        self.output = output;
    }
    return self;
}

#pragma mark -
#pragma mark - Public Methods

#pragma mark - GDLaunchViewModelInput

- (void)showListButtonClickedWithSelectedIndex:(NSInteger)selectedIndex {
    GDListType listType = [self selectedIndexToListType:selectedIndex];
    switch (listType) {
        case GDListTypeMusicVideo:
            [self.output launchViewModelShouldShowMusicVideosList];
            break;
        case GDListTypeMovie:
            [self.output launchViewModelShouldShowMoviesList];
            break;
    }
}

#pragma mark - GDViewControllerObserver

- (void)viewDidLoad {
    
}

- (void)viewWillAppear {
    
}

- (void)viewDidAppear {
    
}

- (void)viewWillDisappear {
    
}

- (void)viewDidDisappear {
    
}

#pragma mark -
#pragma mark - Private Methods

- (GDListType)selectedIndexToListType:(NSInteger)selectedIndex {
    if (GDListTypeMusicVideo == selectedIndex) {
        return GDListTypeMusicVideo;
    } else if (GDListTypeMovie == selectedIndex) {
        return GDListTypeMovie;
    }
    
    return GDListTypeMovie;
}

@end
