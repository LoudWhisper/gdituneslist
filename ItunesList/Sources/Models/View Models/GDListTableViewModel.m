//
//  GDListTableViewModel.m
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDListTableViewModel.h"

#import "GDListTableViewCell.h"

@interface GDListTableViewModel() <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation GDListTableViewModel

#pragma mark -
#pragma mark - Protocols Implementation

#pragma mark - UITableViewDelegate & UITableViewDataSource

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.onSelect && self.listModels && [self.listModels count] > indexPath.row) {
        GDListModel *model = [self.listModels objectAtIndex:indexPath.row];
        self.onSelect(model);
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.listModels count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.listModels && [self.listModels count] > indexPath.row) {
        GDListModel *model = [self.listModels objectAtIndex:indexPath.row];
        
        GDListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([GDListTableViewCell class])];
        [cell configureWithListModel:model];
        return cell;
    } else {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NSStringFromClass([UITableViewCell class])];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.listModels && [self.listModels count] > indexPath.row) {
        return 80.0;
    } else {
        return CGFLOAT_MIN;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

@end
