//
//  GDDetailListViewModel.h
//  ItunesList
//
//  Created by Daniil on 15.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GDDetailListViewModelInputOutput.h"

@class GDListModel;

@interface GDDetailListViewModel : NSObject <GDDetailListViewModelInput>

/**
 Initialize view model with selected list model and output delegate.

 @param listModel iTunes item.
 @param output Object which conforms to GDDetailListViewModelOutput.
 @return Instance of GDListViewModel.
 */
- (instancetype)initWithListModel:(GDListModel *)listModel
                           output:(id<GDDetailListViewModelOutput>)output NS_DESIGNATED_INITIALIZER;

- (instancetype)init __attribute__((unavailable("'init' not available, use 'initWithListModel:output:' instead.")));

@end
