//
//  GDLaunchViewModel.h
//  ItunesList
//
//  Created by Daniil on 15.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GDLaunchViewModelInputOutput.h"

@interface GDLaunchViewModel : NSObject <GDLaunchViewModelInput>

/**
 Initialize view model with output delegate.

 @param output Object which conforms to GDLaunchViewModelOutput.
 @return Instance of GDLaunchViewModel.
 */
- (instancetype)initWithOutput:(id<GDLaunchViewModelOutput>)output NS_DESIGNATED_INITIALIZER;

- (instancetype)init __attribute__((unavailable("'init' not available, use 'initWithOutput:' instead.")));

@end
