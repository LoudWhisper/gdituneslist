//
//  GDListTableViewModel.h
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GDListModel;

@interface GDListTableViewModel : NSObject

/**
 Array of GDListModel to show in the table view.
 */
@property (nonatomic, strong) NSArray<GDListModel *> *listModels;

/**
 Called after cell selected.
 */
@property (nonatomic, copy) void (^onSelect)(GDListModel *model);

@end
