//
//  GDListViewModel.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 13.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDListViewModel.h"

#import "GDListDataModel.h"

#import "NSString+GDTexts.h"

@interface GDListViewModel ()

@property (nonatomic, strong) GDListDataModel *dataModel;
@property (nonatomic, weak) id<GDListViewModelOutput> output;

@property (nonatomic) GDListViewModelSearchState searchState;
@property (nonatomic, copy) NSString *inputText;
@property (nonatomic, copy) NSString *searchString;
@property (nonatomic, strong) NSArray *searchResults;

@end

@implementation GDListViewModel

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithListDataModel:(GDListDataModel *)dataModel output:(id<GDListViewModelOutput>)output {
    self = [super init];
    if (self) {
        self.searchState = GDListViewModelSearchStateInactive;
        
        self.dataModel = dataModel;
        self.output = output;
    }
    return self;
}

#pragma mark -
#pragma mark - Public Methods

#pragma mark - GDListViewModelInput

- (void)startSearchButtonClicked {
    [self.dataModel cancelSearch];
    
    [self searchWithString:self.inputText];
    [self searchResultsChanged:nil withError:nil];
    [self searchStateChanged:GDListViewModelSearchStateActive];
}

- (void)cancelSearchButtonClicked {
    [self.dataModel cancelSearch];
    
    self.searchString = nil;
    
    [self searchResultsChanged:nil withError:nil];
    [self searchStateChanged:GDListViewModelSearchStateCancelled];
}

- (void)inputTextChanged:(NSString *)text {
    self.inputText = text;
}

#pragma mark - GDViewControllerObserver

- (void)viewDidLoad {
    
}

- (void)viewWillAppear {
    [self.output listViewModelSearchResultsChanged:self.searchResults];
    [self.output listViewModelSearchStateChanged:self.searchState];
}

- (void)viewDidAppear {
    
}

- (void)viewWillDisappear {
    
}

- (void)viewDidDisappear {
    
}

#pragma mark -
#pragma mark - Private Methods

#pragma mark - Search Methods

- (void)searchWithString:(NSString *)string {
    self.searchString = string;
    
    __weak GDListViewModel *wSelf = self;
    [self.dataModel searchWithString:string completion:^(NSString *error, NSArray<GDListModel *> *results) {
        __strong GDListViewModel *sSelf = wSelf;
        if ([results count] == 0 && [string length] > 0) {
            [sSelf searchFailedWithString:string requestError:error];
        } else {
            [sSelf searchResultsChanged:results withError:error];
            [sSelf searchStateChanged:GDListViewModelSearchStateInactive];
        }
    }];
}

- (void)searchFailedWithString:(NSString *)string requestError:(NSString *)requestError {
    if (!self.searchString || !string || ![self.searchString isEqualToString:string]) {
        return;
    }
    
    __weak GDListViewModel *wSelf = self;
    [self.dataModel loadCachedWithString:string completion:^(NSString *error, NSArray<GDListModel *> *results) {
        __strong GDListViewModel *sSelf = wSelf;
        [sSelf cacheLoadedWithString:string requestError:requestError results:results];
    }];
}

- (void)cacheLoadedWithString:(NSString *)string requestError:(NSString *)requestError results:(NSArray *)results {
    if (!self.searchString || !string || ![self.searchString isEqualToString:string]) {
        return;
    }
    
    [self searchResultsChanged:results withError:requestError];
    [self searchStateChanged:GDListViewModelSearchStateInactive];
}

#pragma mark - Support Methods

- (void)searchStateChanged:(GDListViewModelSearchState)state {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.searchState = state;
        [self.output listViewModelSearchStateChanged:state];
    });
}

- (void)searchResultsChanged:(NSArray *)searchResults withError:(NSString *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.searchResults = searchResults;
        [self.output listViewModelSearchResultsChanged:searchResults];
        
        if (error && [error length] > 0) {
            NSString *errorTitle = [NSString gd_title_error_alert];
            [self.output listViewModelDidReceiveErrorWithTitle:errorTitle message:error];
        }
    });
}

@end
