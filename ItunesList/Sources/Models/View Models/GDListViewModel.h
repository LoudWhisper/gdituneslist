//
//  GDListViewModel.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 13.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GDListViewModelInputOutput.h"

@class GDListDataModel;

@interface GDListViewModel : NSObject <GDListViewModelInput>

/**
 Initialize view model with data model and output delegate.

 @param dataModel iTunes list data model.
 @param output Object which conforms to GDListViewModelOutput.
 @return Instance of GDListViewModel.
 */
- (instancetype)initWithListDataModel:(GDListDataModel *)dataModel
                               output:(id<GDListViewModelOutput>)output NS_DESIGNATED_INITIALIZER;

- (instancetype)init __attribute__((unavailable("'init' not available, use 'initWithListDataModel:output:' instead.")));

@end
