//
//  GDCompletionBlocks.h
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#ifndef GDCompletionBlocks_h
#define GDCompletionBlocks_h

@class GDListModel;

typedef void (^GDNetworkCompletionBlock) (BOOL cancelled, NSString *error, NSDictionary *response);

typedef void (^GDDataProviderCompletionBlock) (NSString *error, NSArray<GDListModel *> *results);

#endif /* GDCompletionBlocks_h */
