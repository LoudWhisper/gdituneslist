//
//  GDListModelKeys.m
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDListModelKeys.h"

NSString *const gd_list_artist_name     = @"artistName";

NSString *const gd_list_track_name      = @"trackName";
NSString *const gd_list_track_address   = @"trackViewUrl";

NSString *const gd_list_artwork_address = @"artworkUrl100";
NSString *const gd_list_collection_name = @"collectionName";
NSString *const gd_list_preview_address = @"previewUrl";
