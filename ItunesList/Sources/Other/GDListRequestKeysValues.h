//
//  GDListRequestKeysValues.h
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#ifndef GDListRequestKeysValues_h
#define GDListRequestKeysValues_h

#import <Foundation/Foundation.h>

extern NSString *const gd_list_request_search_string_key;
extern NSString *const gd_list_request_search_type_key;
extern NSString *const gd_list_request_results_key;

extern NSString *const gd_list_request_music_video_value;
extern NSString *const gd_list_request_movie_value;

#endif /* GDListRequestKeysValues_h */
