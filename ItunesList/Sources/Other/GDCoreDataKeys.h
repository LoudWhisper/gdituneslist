//
//  GDCoreDataKeys.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#ifndef GDListModelKeys_h
#define GDListModelKeys_h

#import <Foundation/Foundation.h>

extern NSString *const gd_response_data_key;

extern NSString *const gd_type_key;
extern NSString *const gd_timestamp_key;
extern NSString *const gd_search_string_key;
extern NSString *const gd_responses_key;

#endif /* GDListModelKeys_h */
