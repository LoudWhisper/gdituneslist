//
//  GDListTypes.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#ifndef GDListTypes_h
#define GDListTypes_h

typedef NS_ENUM(NSUInteger, GDListType) {
    GDListTypeMusicVideo,
    GDListTypeMovie
};

#endif /* GDListTypes_h */
