//
//  GDExceptionKeys.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDExceptionKeys.h"

NSString *const gd_view_controller_key              = @"viewController";
NSString *const gd_view_controller_identifier_key   = @"viewControllerIdentifier";

NSString *const gd_storyboard_key                   = @"storyboard";
NSString *const gd_storyboard_name_key              = @"storyboardName";

NSString *const gd_query_name_key                   = @"name";
NSString *const gd_query_value_key                  = @"value";

NSString *const gd_scheme_key                       = @"scheme";
NSString *const gd_host_key                         = @"host";
NSString *const gd_path_key                         = @"path";
NSString *const gd_parameters_key                   = @"parameters";

NSString *const gd_core_data_name_key               = @"modelName";
NSString *const gd_core_data_entity_name_key        = @"entityName";
NSString *const gd_core_data_entity_key             = @"entity";
