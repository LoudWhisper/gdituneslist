//
//  GDExceptionKeys.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#ifndef GDExceptionKeys_h
#define GDExceptionKeys_h

#import <Foundation/Foundation.h>

extern NSString *const gd_view_controller_key;
extern NSString *const gd_view_controller_identifier_key;

extern NSString *const gd_storyboard_key;
extern NSString *const gd_storyboard_name_key;

extern NSString *const gd_query_name_key;
extern NSString *const gd_query_value_key;

extern NSString *const gd_scheme_key;
extern NSString *const gd_host_key;
extern NSString *const gd_path_key;
extern NSString *const gd_parameters_key;

extern NSString *const gd_core_data_name_key;
extern NSString *const gd_core_data_entity_name_key;
extern NSString *const gd_core_data_entity_key;

#endif /* GDExceptionKeys_h */
