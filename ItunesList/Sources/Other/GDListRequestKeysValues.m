//
//  GDListRequestKeysValues.m
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDListRequestKeysValues.h"

NSString *const gd_list_request_search_string_key   = @"term";
NSString *const gd_list_request_search_type_key     = @"entity";
NSString *const gd_list_request_results_key         = @"results";

NSString *const gd_list_request_music_video_value   = @"musicVideo";
NSString *const gd_list_request_movie_value         = @"movie";
