//
//  GDListModelKeys.h
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#ifndef GDListModelKeys_h
#define GDListModelKeys_h

#import <Foundation/Foundation.h>

extern NSString *const gd_list_artist_name;

extern NSString *const gd_list_track_name;
extern NSString *const gd_list_track_address;

extern NSString *const gd_list_artwork_address;
extern NSString *const gd_list_collection_name;
extern NSString *const gd_list_preview_address;

#endif /* GDListModelKeys_h */
