//
//  GDCoreDataKeys.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDCoreDataKeys.h"

NSString *const gd_response_data_key    = @"responseData";

NSString *const gd_type_key             = @"type";
NSString *const gd_timestamp_key        = @"timestamp";
NSString *const gd_search_string_key    = @"search_string";
NSString *const gd_responses_key        = @"responses";
