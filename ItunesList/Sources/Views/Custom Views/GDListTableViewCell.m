//
//  GDListTableViewCell.m
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDListTableViewCell.h"

#import <SDWebImage/UIView+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "GDListModel.h"
#import "UIImage+GDImages.h"

@interface GDListTableViewCell ()

@property (nonatomic, weak) IBOutlet UIImageView *artworkImageView;
@property (nonatomic, weak) IBOutlet UILabel *artistNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *collectionNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *trackNameLabel;

@end

@implementation GDListTableViewCell

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark -
#pragma mark - Public Methods

- (void)configureWithListModel:(GDListModel *)model {
    [self.artistNameLabel setText:model.artistName];
    [self.collectionNameLabel setText:model.collectionName];
    [self.trackNameLabel setText:model.trackName];
    
    if (model.artworkAddress && [model.artworkAddress length] > 0 && [NSURL URLWithString:model.artworkAddress]) {
        [self.artworkImageView sd_setImageWithURL:[NSURL URLWithString:model.artworkAddress] placeholderImage:[UIImage gd_artwork_placeholder] options:SDWebImageRetryFailed];
    } else {
        [self.artworkImageView sd_cancelCurrentImageLoad];
        [self.artworkImageView setImage:[UIImage gd_artwork_placeholder]];
    }
}

@end
