//
//  GDListPlaceholderView.h
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDUntouchableXibView.h"

@interface GDListPlaceholderView : GDUntouchableXibView

@end
