//
//  GDListTableViewCell.h
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GDListModel;

@interface GDListTableViewCell : UITableViewCell

/**
 Configures cell with iTunes item.

 @param model iTunes item.
 */
- (void)configureWithListModel:(GDListModel *)model;

@end
