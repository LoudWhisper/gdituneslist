//
//  GDDesignableButton.h
//  ItunesList
//
//  Created by Daniil on 26.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface GDDesignableButton : UIButton

@end
