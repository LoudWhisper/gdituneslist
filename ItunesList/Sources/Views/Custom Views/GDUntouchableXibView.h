//
//  GDUntouchableXibView.h
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GDUntouchableXibView : UIView

/**
 Adds view to superview and configures constraints.

 @param superview Superview to add a view.
 */
- (void)addAsSubviewTo:(UIView *)superview;

@end
