//
//  GDUntouchableXibView.m
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDUntouchableXibView.h"

@interface GDUntouchableXibView ()

@end

@implementation GDUntouchableXibView

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupContentView];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupContentView];
    }
    return self;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    return NO;
}

#pragma mark -
#pragma mark - Public Methods

- (void)addAsSubviewTo:(UIView *)superview {
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    [superview addSubview:self];
    
    NSDictionary *views = @{@"view" : self,
                            @"superview" : superview};
    
    NSMutableArray *constraints = [NSMutableArray new];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view(superview)]-0-|" options:kNilOptions metrics:nil views:views]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view(superview)]-0-|" options:kNilOptions metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:constraints];
}

#pragma mark -
#pragma mark - Private Methods

- (void)setupContentView {
    id loadedView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    [loadedView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [loadedView setFrame:self.bounds];
    [self addSubview:loadedView];
    
    NSMutableArray *constraints = [NSMutableArray new];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[loadedView]-0-|" options:kNilOptions metrics:nil views:@{@"loadedView" : loadedView}]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[loadedView]-0-|" options:kNilOptions metrics:nil views:@{@"loadedView" : loadedView}]];
    [NSLayoutConstraint activateConstraints:constraints];
}

@end
