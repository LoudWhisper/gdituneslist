//
//  GDDesignableButton.m
//  ItunesList
//
//  Created by Daniil on 26.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDDesignableButton.h"

#import "UIView+GDDesignables.h"

@implementation GDDesignableButton

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configureDesignableView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configureDesignableView];
    }
    return self;
}

@end
