//
//  GDCoreDataManager.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDCoreDataManager.h"

#import <CoreData/CoreData.h>

#import "GDListModel.h"
#import "GDCoreDataKeys.h"
#import "GDExceptionFabric.h"
#import "GDLogger.h"

@interface GDCoreDataManager ()

@property (nonatomic, strong) GDExceptionFabric *exceptionFabric;
@property (nonatomic) NSInteger maximumCacheRequestCount;
@property (nonatomic, strong) NSManagedObjectContext *writeContext;

@end

@implementation GDCoreDataManager

static NSString *const gd_core_data_model_name          = @"GDListCoreDataModel";
static NSString *const gd_request_entity_name           = @"GDListRequestEntity";
static NSString *const gd_request_response_entity_name  = @"GDListRequestResponseEntity";

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithMaximumCachedRequestsCount:(NSInteger)maximumCacheRequestCount {
    self = [super init];
    if (self) {
        self.exceptionFabric = [GDExceptionFabric new];
        self.maximumCacheRequestCount = maximumCacheRequestCount;
        [self setupCoreDataStack];
    }
    return self;
}

#pragma mark -
#pragma mark - Public Methods

- (void)saveRequestWithSearchString:(NSString *)searchString type:(NSUInteger)type results:(NSArray<GDListModel *> *)results {
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [context setParentContext:self.writeContext];
    
    [context performBlockAndWait:^{
        NSEntityDescription *requestEntity = [self requestEntityInContext:context];
        NSEntityDescription *requestResponseEntity = [self requestResponseEntityInContext:context];
        
        if (self.maximumCacheRequestCount > 0) {
            NSUInteger cachedRequestsCount = [self cachedRequestsCountInContext:context];
            if (cachedRequestsCount >= self.maximumCacheRequestCount) {
                [self deleteCachedRequestsOverLimitInContext:context];
            }
        }
        
        NSMutableOrderedSet *responseModels = [NSMutableOrderedSet new];
        for (GDListModel *model in results) {
            NSManagedObject *responseModel = [[NSManagedObject alloc] initWithEntity:requestResponseEntity insertIntoManagedObjectContext:context];
            [responseModel setValue:model forKey:gd_response_data_key];
            [responseModels addObject:responseModel];
        }
        
        NSTimeInterval timestamp = [[NSDate date] timeIntervalSince1970];
        NSManagedObject *requestModel = [self cachedRequestModelWithSearchString:searchString type:type inContext:context withEntity:requestEntity createEntityIfNeeded:YES];
        [requestModel setValue:@(type) forKey:gd_type_key];
        [requestModel setValue:searchString forKey:gd_search_string_key];
        [requestModel setValue:@(timestamp) forKey:gd_timestamp_key];
        [requestModel setValue:responseModels forKey:gd_responses_key];
        
        [self saveChangesInContext:context];
        [self.writeContext performBlockAndWait:^{
            [self saveChangesInContext:self.writeContext];
            
            [GDLogger log:[NSString stringWithFormat:@"core data save request finished with search string %@ and results count %@", searchString, @([results count])] withMode:GDLogModeDebug];
        }];
    }];
}

- (void)loadCachedResultsWithSearchString:(NSString *)searchString type:(NSUInteger)type completion:(void (^)(NSArray<GDListModel *> *results))completion {
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [context setParentContext:self.writeContext];
    
    [context performBlockAndWait:^{
        NSManagedObject *requestModel = [self cachedRequestModelWithSearchString:searchString type:type inContext:context withEntity:nil createEntityIfNeeded:NO];
        if (requestModel) {
            NSOrderedSet *responseModels = [requestModel valueForKey:gd_responses_key];
            
            NSMutableArray *results = [NSMutableArray new];
            for (NSManagedObject *responseModel in [responseModels array]) {
                GDListModel *model = [responseModel valueForKey:gd_response_data_key];
                if (model) {
                    [results addObject:model];
                }
            }
            
            if (completion) {
                completion(results);
            }
        } else {
            if (completion) {
                completion(nil);
            }
        }
    }];
}

#pragma mark -
#pragma mark - Private Methods

#pragma mark - Setup Methods

- (void)setupCoreDataStack {
    NSPersistentContainer *persistentContainer = [[NSPersistentContainer alloc] initWithName:gd_core_data_model_name];
    if (persistentContainer) {
        [persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *description, NSError *error) {
            [GDLogger log:[NSString stringWithFormat:@"core data persistent container did load stores with description: %@ and error: %@", description, error] withMode:GDLogModeDebug];
        }];
    } else {
        @throw [self.exceptionFabric exceptionForCoreDataModelName:gd_core_data_model_name];
    }
    
    self.writeContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [self.writeContext setPersistentStoreCoordinator:[persistentContainer persistentStoreCoordinator]];
}

#pragma mark - Core Data Methods

- (NSUInteger)cachedRequestsCountInContext:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:gd_request_entity_name];
    [fetchRequest setIncludesSubentities:NO];
    [fetchRequest setIncludesPropertyValues:NO];
    
    NSError *error;
    NSUInteger count = [context countForFetchRequest:fetchRequest error:&error];
    
    [GDLogger log:[NSString stringWithFormat:@"core data count request finished with error %@", error] withMode:GDLogModeDebug];
    
    if (count == NSNotFound) {
        return 0;
    }
    return count;
}

- (void)deleteCachedRequestsOverLimitInContext:(NSManagedObjectContext *)context {
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:gd_timestamp_key ascending:false];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:gd_request_entity_name];
    [fetchRequest setResultType:NSManagedObjectIDResultType];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    [fetchRequest setFetchOffset:(self.maximumCacheRequestCount - 1)];
    
    NSError *fetchError;
    NSArray *results = [context executeFetchRequest:fetchRequest error:&fetchError];

    [GDLogger log:[NSString stringWithFormat:@"core data fetch to delete request finished with results count %@ and error %@", @([results count]), fetchError] withMode:GDLogModeDebug];
    
    if (results && [results count] > 0) {
        NSError *deleteError;
        NSBatchDeleteRequest *deleteRequest = [[NSBatchDeleteRequest alloc] initWithObjectIDs:results];
        [deleteRequest setResultType:NSBatchDeleteResultTypeCount];
        NSBatchDeleteResult *deleteResult = [context executeRequest:deleteRequest error:&deleteError];
        
        [GDLogger log:[NSString stringWithFormat:@"core data delete request finished with result %@ and error %@", [deleteResult result], deleteError] withMode:GDLogModeDebug];
    }
}

- (NSManagedObject *)cachedRequestModelWithSearchString:(NSString *)searchString type:(NSUInteger)type inContext:(NSManagedObjectContext *)context withEntity:(NSEntityDescription *)entity createEntityIfNeeded:(BOOL)createEntityIfNeeded {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %@ AND %K = %@", gd_search_string_key, searchString, gd_type_key, @(type)];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:gd_request_entity_name];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setFetchLimit:1];
    
    NSError *error;
    NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
    
    [GDLogger log:[NSString stringWithFormat:@"core data fetch request finished with results count %@ and error %@", @([results count]), error] withMode:GDLogModeDebug];
    
    if ([results count] > 0) {
        return [results firstObject];
    } else if (createEntityIfNeeded) {
        NSManagedObject *cachedRequestModel = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
        return cachedRequestModel;
    }
    
    return nil;
}

- (void)saveChangesInContext:(NSManagedObjectContext *)context {
    if (![context hasChanges]) {
        return;
    }
    
    NSError *error;
    [context save:&error];
    
    [GDLogger log:[NSString stringWithFormat:@"core data context saved error %@", error] withMode:GDLogModeDebug];
}

#pragma mark - Entities

- (NSEntityDescription *)requestEntityInContext:(NSManagedObjectContext *)context {
    NSEntityDescription *requestEntity = [NSEntityDescription entityForName:gd_request_entity_name inManagedObjectContext:context];
    if (requestEntity) {
        return requestEntity;
    }
    
    @throw [self.exceptionFabric exceptionForCoreDataModelName:gd_core_data_model_name entityName:gd_request_entity_name entity:requestEntity];
}

- (NSEntityDescription *)requestResponseEntityInContext:(NSManagedObjectContext *)context {
    NSEntityDescription *requestResponseEntity = [NSEntityDescription entityForName:gd_request_response_entity_name inManagedObjectContext:context];
    if (requestResponseEntity) {
        return requestResponseEntity;
    }
    
    @throw [self.exceptionFabric exceptionForCoreDataModelName:gd_core_data_model_name entityName:gd_request_response_entity_name entity:requestResponseEntity];
}

@end
