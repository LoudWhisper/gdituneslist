//
//  GDSessionManager.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDSessionManager.h"

#import "GDLogger.h"

#import "NSString+GDTexts.h"

@interface GDSessionManager ()

@property (nonatomic, strong) NSURLSession *session;

@end

@implementation GDSessionManager

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)init {
    self = [super init];
    if (self) {
        self.session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    }
    return self;
}

#pragma mark -
#pragma mark - Public Methods

- (NSURLSessionDataTask *)runRequest:(NSURLRequest *)request completion:(GDNetworkCompletionBlock)completion {
    __weak GDSessionManager *wSelf = self;
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        __strong GDSessionManager *sSelf = wSelf;
        [sSelf perform:completion withData:data response:response error:error];
    }];
    [task resume];
    return task;
}

#pragma mark -
#pragma mark - Private Methods

#pragma mark - Process Response

- (void)perform:(GDNetworkCompletionBlock)completion withData:(NSData *)data response:(NSURLResponse *)response error:(NSError *)error {
    [GDLogger log:[NSString stringWithFormat:@"complete request with url: %@ and error: %@", response.URL, error] withMode:GDLogModeDebug];
    
    NSDictionary *serializedObject = [self serializedObjectFromData:data];
    NSString *errorMessage = [self errorMessageWithSerializedObject:serializedObject response:response error:error];
    BOOL requestCancelled = ([error code] == NSURLErrorCancelled);
    
    if (completion) {
        completion(requestCancelled, errorMessage, serializedObject);
    }
}

- (NSDictionary *)serializedObjectFromData:(NSData *)data {
    if (data != nil) {
        NSError *error;
        NSDictionary *serializedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        if (serializedObject && [serializedObject isKindOfClass:[NSDictionary class]]) {
            [GDLogger log:[NSString stringWithFormat:@"complete serialization of data with length: %@ to serialized object: %@ with error: %@", @([data length]), serializedObject, error] withMode:GDLogModeDebug];
            return serializedObject;
        } else {
            NSString *serializedString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            [GDLogger log:[NSString stringWithFormat:@"failed serialization of data with length: %@ to serialized object: %@ and serialized string: %@ with error: %@", @([data length]), serializedObject, serializedString, error] withMode:GDLogModeDebug];
        }
    } else {
        [GDLogger log:[NSString stringWithFormat:@"failed serialization of data with length: %@", @([data length])] withMode:GDLogModeDebug];
    }
    
    return nil;
}

- (NSString *)errorMessageWithSerializedObject:(NSDictionary *)serializedObject response:(NSURLResponse *)response error:(NSError *)error {
    if (response && [response isKindOfClass:[NSHTTPURLResponse class]]) {
        NSUInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
        if (statusCode == 0) {
            return [NSString gd_error_message_request_timedout];
        } else if (statusCode >= 300 && error == nil) {
            return [NSString gd_error_message_request_failed];
        }
    }
    
    if (error) {
        if ([error localizedFailureReason]) {
            return [error localizedFailureReason];
        } else if ([error localizedDescription]) {
            return [error localizedDescription];
        } else {
            return [NSString gd_error_message_request_failed];
        }
    }
    
    return nil;
}

@end
