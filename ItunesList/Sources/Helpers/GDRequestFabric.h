//
//  GDRequestFabric.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GDRequestFabric : NSObject

/**
 Creates network request.

 @param scheme Endpoint scheme.
 @param host Endpoint host.
 @param path Endpoint path.
 @param queryParameters Endpoint query parameters.
 @return Instance of NSURLRequest.
 */
- (NSURLRequest *)requestWithScheme:(NSString *)scheme
                               host:(NSString *)host
                               path:(NSString *)path
                    queryParameters:(NSDictionary *)queryParameters;

@end
