//
//  GDEndpointFabric.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GDEndpointFabric : NSObject

/**
 Creates URL with endpoint elements.

 @param scheme Endpoint scheme.
 @param host Endpoint host.
 @param path Endpoint path.
 @param queryParameters Endpoint query parameters.
 @return Instance of NSURL.
 */
- (NSURL *)urlWithScheme:(NSString *)scheme
                    host:(NSString *)host
                    path:(NSString *)path
         queryParameters:(NSDictionary *)queryParameters;

@end
