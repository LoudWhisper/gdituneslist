//
//  GDEndpointFabric.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDEndpointFabric.h"

#import "GDExceptionFabric.h"

@interface GDEndpointFabric ()

@property (nonatomic, strong) GDExceptionFabric *exceptionFabric;

@end

@implementation GDEndpointFabric

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)init {
    self = [super init];
    if (self) {
        self.exceptionFabric = [GDExceptionFabric new];
    }
    return self;
}

#pragma mark -
#pragma mark - Public Methods

- (NSURL *)urlWithScheme:(NSString *)scheme host:(NSString *)host path:(NSString *)path queryParameters:(NSDictionary *)parameters {
    NSMutableArray *queryItems = [NSMutableArray new];
    [parameters enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        NSURLQueryItem *item = [self queryItemWithName:key value:obj];
        [queryItems addObject:item];
    }];
    
    NSURLComponents *components = [self componentsWithScheme:scheme host:host path:path queryItems:queryItems];
    NSURL *url = [components URL];
    if (url && [url scheme] && [url host]) {
        return url;
    }
    
    @throw [self.exceptionFabric exceptionForScheme:scheme host:host path:path queryParameters:parameters];
}

#pragma mark -
#pragma mark - Private Methods

#pragma mark - Build URL Components

- (NSURLComponents *)componentsWithScheme:(NSString *)scheme host:(NSString *)host path:(NSString *)path queryItems:(NSArray<NSURLQueryItem *> *)items {
    NSURLComponents *components = [NSURLComponents new];
    [components setScheme:scheme];
    [components setHost:host];
    [components setPath:path];
    [components setQueryItems:items];
    return components;
}

- (NSURLQueryItem *)queryItemWithName:(NSString *)name value:(NSString *)value {
    NSString *queryName = [self queryNameFrom:name];
    NSString *queryValue = [self queryValueFrom:value];
    if (queryName && [queryName length] > 0 && queryValue && [queryValue length] > 0) {
        NSURLQueryItem *queryItem = [[NSURLQueryItem alloc] initWithName:queryName value:queryValue];
        return queryItem;
    }
    
    @throw [self.exceptionFabric exceptionForQueryName:name queryValue:value];
}

#pragma mark - Support Methods

- (NSString *)queryNameFrom:(id)object {
    if (object && [object isKindOfClass:[NSString class]]) {
        return object;
    }
    return nil;
}

- (NSString *)queryValueFrom:(id)object {
    if (object && [object isKindOfClass:[NSString class]]) {
        NSCharacterSet *queryCharacterSet = [NSCharacterSet URLQueryAllowedCharacterSet];
        return [object stringByAddingPercentEncodingWithAllowedCharacters:queryCharacterSet];
    }
    return nil;
}

@end
