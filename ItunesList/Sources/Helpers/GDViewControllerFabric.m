//
//  GDViewControllerFabric.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDViewControllerFabric.h"

#import "GDExceptionFabric.h"

@interface GDViewControllerFabric ()

@property (nonatomic, strong) GDExceptionFabric *exceptionFabric;

@end

@implementation GDViewControllerFabric

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)init {
    self = [super init];
    if (self) {
        self.exceptionFabric = [GDExceptionFabric new];
    }
    return self;
}

#pragma mark -
#pragma mark - Public Methods

- (__kindof UIViewController *)createViewControllerWithIdentifier:(NSString *)identifier fromStoryboardWithType:(GDStoryboardType)type {
    NSString *storyboardName = [self stringForType:type];
    UIStoryboard *storyboard = [self storyboardWithName:storyboardName];
    if (storyboard) {
        UIViewController *viewController = [self viewControllerWithIdentifier:identifier fromStoryboard:storyboard];
        if (viewController) {
            return viewController;
        }
    }
    
    @throw [self.exceptionFabric exceptionForViewController:nil viewControllerIdentifier:identifier storyboard:storyboard storyboardName:storyboardName];
}

#pragma mark -
#pragma mark - Private Methods

#pragma mark - Setup Storyboards And View Controllers

- (UIStoryboard *)storyboardWithName:(NSString *)name {
    if (!name || ![name isKindOfClass:[NSString class]]) {
        return nil;
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:name bundle:[NSBundle mainBundle]];
    return storyboard;
}

- (UIViewController *)viewControllerWithIdentifier:(NSString *)identifier fromStoryboard:(UIStoryboard *)storyboard {
    if (!identifier || ![identifier isKindOfClass:[NSString class]]) {
        return nil;
    }
    
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:identifier];
    return viewController;
}

#pragma mark - Support Methods

- (NSString *)stringForType:(GDStoryboardType)type {
    switch (type) {
        case GDStoryboardTypeLaunch:
            return @"LaunchScreens";
        case GDStoryboardTypeList:
            return @"ListScreens";
    }
    
    return nil;
}

@end
