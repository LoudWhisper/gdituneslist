//
//  GDCoreDataManager.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GDListModel;

@interface GDCoreDataManager : NSObject

/**
 Initialize with maximum cached requests count. There are no restrictions if the count is less or equal to zero.

 @param maximumCacheRequestCount Maximum cached requests count.
 @return Instance of GDCoreDataManager.
 */
- (instancetype)initWithMaximumCachedRequestsCount:(NSInteger)maximumCacheRequestCount NS_DESIGNATED_INITIALIZER;

- (instancetype)init __attribute__((unavailable("'init' not available, use 'initWithMaximumCachedRequestsCount:' instead.")));

/**
 Saves the search request and result models. The number of cached requests is limited. After the overflow the oldest requests will be deleted.
 The requests without a search string and/or models won't be saved.

 @param searchString Search string.
 @param type iTunes list type.
 @param results Search request result models.
 */
- (void)saveRequestWithSearchString:(NSString *)searchString
                               type:(NSUInteger)type
                            results:(NSArray<GDListModel *> *)results;

/**
 Loads cached result models.

 @param searchString Search string.
 @param type iTunes list type.
 @param completion Load completion block.
 */
- (void)loadCachedResultsWithSearchString:(NSString *)searchString
                                     type:(NSUInteger)type
                               completion:(void (^)(NSArray<GDListModel *> *results))completion;

@end
