//
//  GDSessionManager.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GDCompletionBlocks.h"

@interface GDSessionManager : NSObject

/**
 Creates and runs data task with network request.

 @param request Network request.
 @param completion Network request completion block.
 @return Instance of NSURLSessionDataTask.
 */
- (NSURLSessionDataTask *)runRequest:(NSURLRequest *)request
                          completion:(GDNetworkCompletionBlock)completion;

@end
