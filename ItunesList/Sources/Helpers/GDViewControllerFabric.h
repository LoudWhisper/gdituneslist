//
//  GDViewControllerFabric.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, GDStoryboardType) {
    GDStoryboardTypeLaunch,
    GDStoryboardTypeList
};

@interface GDViewControllerFabric : NSObject

/**
 Creates view controller from storyboard with passed identifier.
 
 @param identifier View controller storyboard identifier.
 @param type Storyboard type.
 @return UIViewController instance.
 */
- (__kindof UIViewController *)createViewControllerWithIdentifier:(NSString *)identifier
                                           fromStoryboardWithType:(GDStoryboardType)type;

@end
