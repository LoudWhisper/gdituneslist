//
//  GDExceptionFabric.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GDExceptionFabric : NSObject

/**
 Creates storyboard exception. Throw this exception if your storyboard and/or view controller is invalid.
 
 @param viewController Created view controller.
 @param viewControllerIdentifier Created view controller identifier.
 @param storyboard Created storyboard.
 @param storyboardName Created storyboard name.
 @return NSException instance.
 */
- (NSException *)exceptionForViewController:(id)viewController
                   viewControllerIdentifier:(id)viewControllerIdentifier
                                 storyboard:(id)storyboard
                             storyboardName:(id)storyboardName;

/**
 Creates network exception. Throw this exception if your query name or value is invalid.

 @param queryName Query name.
 @param queryValue Query value.
 @return NSException instance.
 */
- (NSException *)exceptionForQueryName:(id)queryName
                            queryValue:(id)queryValue;

/**
 Creates network exception. Throw this exception if your query name or value is invalid.

 @param scheme Endpoint scheme.
 @param host Endpoint host.
 @param path Endpoint path.
 @param parameters Endpoint query parameters.
 @return NSException instance.
 */
- (NSException *)exceptionForScheme:(id)scheme
                               host:(id)host
                               path:(id)path
                    queryParameters:(id)parameters;

/**
 Creates database exception. Throw this exception if your database model is invalid.

 @param modelName Database model name.
 @return NSException instance.
 */
- (NSException *)exceptionForCoreDataModelName:(id)modelName;

/**
 Creates database exception. Throw this exception if your database entity is invalid.

 @param modelName Database model name.
 @param entityName Database entity name.
 @param entity Database entity.
 @return NSException instance.
 */
- (NSException *)exceptionForCoreDataModelName:(id)modelName
                                    entityName:(id)entityName
                                        entity:(id)entity;

@end
