//
//  GDLogger.m
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDLogger.h"

@implementation GDLogger

#pragma mark -
#pragma mark - Class Methods

+ (void)log:(NSString *)message withMode:(GDLogMode)mode {
    switch (mode) {
        case GDLogModeDebug:
        #if DEBUG
            NSLog(@"%@", message);
        #endif
            break;
        case GDLogModeStandart:
            NSLog(@"%@", message);
            break;
    }
}

@end
