//
//  GDRequestFabric.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDRequestFabric.h"

#import "GDEndpointFabric.h"
#import "GDExceptionFabric.h"

@interface GDRequestFabric ()

@property (nonatomic, strong) GDEndpointFabric *endpointFabric;
@property (nonatomic, strong) GDExceptionFabric *exceptionFabric;

@end

@implementation GDRequestFabric

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)init {
    self = [super init];
    if (self) {
        self.endpointFabric = [GDEndpointFabric new];
        self.exceptionFabric = [GDExceptionFabric new];
    }
    return self;
}

#pragma mark -
#pragma mark - Public Methods

- (NSURLRequest *)requestWithScheme:(NSString *)scheme host:(NSString *)host path:(NSString *)path queryParameters:(NSDictionary *)queryParameters {
    NSURL *url = [self.endpointFabric urlWithScheme:scheme host:host path:path queryParameters:queryParameters];
    if (url && [url scheme] && [url host]) {
        NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30.0];
        return request;
    }
    
    @throw [self.exceptionFabric exceptionForScheme:scheme host:host path:path queryParameters:queryParameters];
}

@end
