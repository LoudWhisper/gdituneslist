//
//  GDLogger.h
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, GDLogMode) {
    GDLogModeDebug,
    GDLogModeStandart
};

@interface GDLogger : NSObject

/**
 Logs message with one of the modes:
 
 GDLogModeDebug: For debug purposes. Uses NSLog. Prints message if '#if DEBUG' statement is true.
 
 GDLogModeStandart: For common purposes. Uses NSLog.

 @param message Log message string.
 @param mode Log mode.
 */
+ (void)log:(NSString *)message
   withMode:(GDLogMode)mode;

@end
