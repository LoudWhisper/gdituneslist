//
//  GDExceptionFabric.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDExceptionFabric.h"

#import "GDExceptionKeys.h"

typedef NS_ENUM(NSUInteger, GDExceptionName) {
    GDExceptionNameStoryboard,
    GDExceptionNameURLComponents,
    GDExceptionNameCoreData
};

typedef NS_ENUM(NSUInteger, GDExceptionReason) {
    GDExceptionReasonStoryboard,
    GDExceptionReasonViewController,
    GDExceptionReasonURLQuery,
    GDExceptionReasonURLComponents,
    GDExceptionReasonCoreDataModel,
    GDExceptionReasonCoreDataEntity
};

@implementation GDExceptionFabric

#pragma mark -
#pragma mark - Public Methods

- (NSException *)exceptionForViewController:(id)viewController viewControllerIdentifier:(id)viewControllerIdentifier storyboard:(id)storyboard storyboardName:(id)storyboardName {
    NSString *name = [self stringForName:GDExceptionNameStoryboard];
    NSString *reason = [self stringForReason:(storyboard ? GDExceptionReasonStoryboard : GDExceptionReasonViewController)];
    NSDictionary *info = @{gd_storyboard_key : [NSString stringWithFormat:@"%@", storyboard],
                           gd_storyboard_name_key : [NSString stringWithFormat:@"%@", storyboardName],
                           gd_view_controller_key : [NSString stringWithFormat:@"%@", viewController],
                           gd_view_controller_identifier_key : [NSString stringWithFormat:@"%@", viewControllerIdentifier]};
    
    return [NSException exceptionWithName:name reason:reason userInfo:info];
}

- (NSException *)exceptionForQueryName:(id)queryName
                            queryValue:(id)queryValue {
    NSString *name = [self stringForName:GDExceptionNameURLComponents];
    NSString *reason = [self stringForReason:GDExceptionReasonURLQuery];
    NSDictionary *info = @{gd_query_name_key : [NSString stringWithFormat:@"%@", queryName],
                           gd_query_value_key : [NSString stringWithFormat:@"%@", queryValue]};
    
    return [NSException exceptionWithName:name reason:reason userInfo:info];
}

- (NSException *)exceptionForScheme:(id)scheme host:(id)host path:(id)path queryParameters:(id)parameters {
    NSString *name = [self stringForName:GDExceptionNameURLComponents];
    NSString *reason = [self stringForReason:GDExceptionReasonURLComponents];
    NSDictionary *info = @{gd_scheme_key : [NSString stringWithFormat:@"%@", scheme],
                           gd_host_key : [NSString stringWithFormat:@"%@", host],
                           gd_path_key : [NSString stringWithFormat:@"%@", path],
                           gd_parameters_key : [NSString stringWithFormat:@"%@", parameters]};
    
    return [NSException exceptionWithName:name reason:reason userInfo:info];
}

- (NSException *)exceptionForCoreDataModelName:(id)modelName {
    NSString *name = [self stringForName:GDExceptionNameCoreData];
    NSString *reason = [self stringForReason:GDExceptionReasonCoreDataModel];
    NSDictionary *info = @{gd_core_data_name_key : [NSString stringWithFormat:@"%@", modelName]};
    
    return [NSException exceptionWithName:name reason:reason userInfo:info];
}

- (NSException *)exceptionForCoreDataModelName:(id)modelName entityName:(id)entityName entity:(id)entity {
    NSString *name = [self stringForName:GDExceptionNameCoreData];
    NSString *reason = [self stringForReason:GDExceptionReasonCoreDataEntity];
    NSDictionary *info = @{gd_core_data_name_key : [NSString stringWithFormat:@"%@", modelName],
                           gd_core_data_entity_name_key : [NSString stringWithFormat:@"%@", entityName],
                           gd_core_data_entity_key : [NSString stringWithFormat:@"%@", entity]};
    
    return [NSException exceptionWithName:name reason:reason userInfo:info];
}

#pragma mark -
#pragma mark - Private Methods

#pragma mark - Support Methods

- (NSString *)stringForName:(GDExceptionName)name {
    switch (name) {
        case GDExceptionNameStoryboard:
            return @"Storyboard exception";
        case GDExceptionNameURLComponents:
            return @"URL components exception";
        case GDExceptionNameCoreData:
            return @"Core data exception";
    }
    
    return nil;
}

- (NSString *)stringForReason:(GDExceptionReason)reason {
    switch (reason) {
        case GDExceptionReasonStoryboard:
            return @"Storyboard doesn't exist";
        case GDExceptionReasonViewController:
            return @"View controller doesn't exist";
        case GDExceptionReasonURLQuery:
            return @"URL query item invalid";
        case GDExceptionReasonURLComponents:
            return @"URL components invalid";
        case GDExceptionReasonCoreDataModel:
            return @"Core data model invalid";
        case GDExceptionReasonCoreDataEntity:
            return @"Core data entity invalid";
    }
    
    return nil;
}

@end
