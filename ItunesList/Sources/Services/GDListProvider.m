//
//  GDListProvider.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDListProvider.h"

#import "GDListModel.h"
#import "GDRequestFabric.h"
#import "GDSessionManager.h"
#import "GDListRequestKeysValues.h"

@interface GDListProvider ()

@property (nonatomic, strong) GDRequestFabric *requestFabric;
@property (nonatomic, strong) GDSessionManager *sessionManager;

@property (nonatomic, copy) NSString *scheme;
@property (nonatomic, copy) NSString *host;
@property (nonatomic, copy) NSString *path;
@property (nonatomic, strong) NSDictionary *queryParameters;

@property (nonatomic, strong) NSURLSessionDataTask *searchListTask;

@end

@implementation GDListProvider

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithScheme:(NSString *)scheme host:(NSString *)host path:(NSString *)path queryParameters:(NSDictionary *)queryParameters {
    self = [super init];
    if (self) {
        self.requestFabric = [GDRequestFabric new];
        self.sessionManager = [GDSessionManager new];
        
        self.scheme = scheme;
        self.host = host;
        self.path = path;
        self.queryParameters = queryParameters;
    }
    return self;
}

#pragma mark -
#pragma mark - Public Methods

- (void)searchWithString:(NSString *)string completion:(GDDataProviderCompletionBlock)completion {
    [self cancelSearchListTask];
    
    NSMutableDictionary *query = [NSMutableDictionary dictionaryWithDictionary:self.queryParameters];
    [query setObject:string forKey:gd_list_request_search_string_key];
    
    NSURLRequest *request = [self.requestFabric requestWithScheme:self.scheme host:self.host path:self.path queryParameters:query];
    
    __weak GDListProvider *wSelf = self;
    self.searchListTask = [self.sessionManager runRequest:request completion:^(BOOL cancelled, NSString *error, NSDictionary *response) {
        if (cancelled) {
            return;
        }
        
        __strong GDListProvider *sSelf = wSelf;
        [sSelf processListResponse:response error:error completion:completion];
    }];
}

- (void)cancelSearch {
    [self cancelSearchListTask];
}

#pragma mark -
#pragma mark - Private Method

#pragma mark - Process Response

- (void)processListResponse:(NSDictionary *)response error:(NSString *)error completion:(GDDataProviderCompletionBlock)completion {
    NSMutableArray *models = [NSMutableArray new];
    if (response && [response isKindOfClass:[NSDictionary class]]) {
        NSArray *results = [response objectForKey:gd_list_request_results_key];
        if (results && [results isKindOfClass:[NSArray class]]) {
            for (NSDictionary *result in results) {
                GDListModel *model = [[GDListModel alloc] initWithDictionary:result];
                if (model) {
                    [models addObject:model];
                }
            }
        }
    }
    
    if (completion) {
        completion(error, models);
    }
}

#pragma mark - Support Methods

- (void)cancelSearchListTask {
    if (self.searchListTask == nil) {
        return;
    }
    
    if ([self.searchListTask state] == NSURLSessionTaskStateRunning || [self.searchListTask state] == NSURLSessionTaskStateSuspended) {
        [self.searchListTask cancel];
    }
    self.searchListTask = nil;
}

@end
