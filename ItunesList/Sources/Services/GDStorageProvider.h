//
//  GDStorageProvider.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GDListModel;

@interface GDStorageProvider : NSObject

/**
 Initialize with iTunes list type integer value.

 @param listType iTunes list type.
 @return Instance of GDStorageProvider.
 */
- (instancetype)initWithListType:(NSUInteger)listType NS_DESIGNATED_INITIALIZER;

- (instancetype)init __attribute__((unavailable("'init' not available, use 'initWithListType:' instead.")));

/**
 Saves search request with result models.

 @param searchString Search string.
 @param results Search results.
 */
- (void)saveRequestWithSearchString:(NSString *)searchString
                            results:(NSArray<GDListModel *> *)results;

/**
 Loads cached result models.

 @param searchString Search string.
 @param completion Load completion block.
 */
- (void)loadCachedResultsWithSearchString:(NSString *)searchString
                               completion:(void (^)(NSArray<GDListModel *> *results))completion;

@end
