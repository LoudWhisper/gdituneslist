//
//  GDAddressesProvider.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDAddressesProvider.h"

#import "GDiTunesListAddressModel.h"

@implementation GDAddressesProvider

#pragma mark -
#pragma mark - Public Methods

- (id<GDAddressProvider>)musicVideoListAddress {
    return [[GDiTunesListAddressModel alloc] initWithType:GDListTypeMusicVideo];
}

- (id<GDAddressProvider>)movieListAddress {
    return [[GDiTunesListAddressModel alloc] initWithType:GDListTypeMovie];
}

@end
