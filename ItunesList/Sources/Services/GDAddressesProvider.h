//
//  GDAddressesProvider.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GDAddressProvider.h"

@interface GDAddressesProvider : NSObject

/**
 Creates address provider for iTunes music videos list.

 @return Object which conforms to GDAddressProvider.
 */
- (id<GDAddressProvider>)musicVideoListAddress;

/**
 Creates address provider for iTunes movies list.

 @return Object which conforms to GDAddressProvider.
 */
- (id<GDAddressProvider>)movieListAddress;

@end
