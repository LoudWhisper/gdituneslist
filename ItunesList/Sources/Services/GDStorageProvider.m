//
//  GDStorageProvider.m
//  ItunesList
//
//  Created by Daniil Gavrilov on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDStorageProvider.h"

#import "GDListModel.h"
#import "GDCoreDataManager.h"

@interface GDStorageProvider ()

@property (nonatomic, strong) GDCoreDataManager *coreDataManager;
@property (nonatomic) dispatch_queue_t serialQueue;

@property (nonatomic) NSUInteger listType;

@end

@implementation GDStorageProvider

static NSUInteger const gd_maximum_cached_requests  = 3;

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (instancetype)initWithListType:(NSUInteger)listType {
    self = [super init];
    if (self) {
        self.coreDataManager = [[GDCoreDataManager alloc] initWithMaximumCachedRequestsCount:gd_maximum_cached_requests];
        self.serialQueue = dispatch_queue_create("com.storageprovider.serialqueue", DISPATCH_QUEUE_SERIAL);
        
        self.listType = listType;
    }
    return self;
}

#pragma mark -
#pragma mark - Public Methods

- (void)saveRequestWithSearchString:(NSString *)searchString results:(NSArray<GDListModel *> *)results {
    if ([results count] == 0 || [searchString length] == 0) {
        return;
    }
    
    __weak GDStorageProvider *wSelf = self;
    dispatch_async(self.serialQueue, ^{
        __strong GDStorageProvider *sSelf = wSelf;
        [[sSelf coreDataManager] saveRequestWithSearchString:searchString type:self.listType results:results];
    });
}

- (void)loadCachedResultsWithSearchString:(NSString *)searchString completion:(void (^)(NSArray<GDListModel *> *results))completion {
    __weak GDStorageProvider *wSelf = self;
    dispatch_async(self.serialQueue, ^{
        __strong GDStorageProvider *sSelf = wSelf;
        [[sSelf coreDataManager] loadCachedResultsWithSearchString:searchString type:self.listType completion:completion];
    });
}

@end
