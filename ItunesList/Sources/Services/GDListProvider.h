//
//  GDListProvider.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 12.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GDCompletionBlocks.h"

@interface GDListProvider : NSObject

/**
 Initialize with endpoint elements.

 @param scheme Endpoint scheme.
 @param host Endpoint host.
 @param path Endpoint path.
 @param queryParameters Endpoint query parameters.
 @return Instance of GDListProvider.
 */
- (instancetype)initWithScheme:(NSString *)scheme
                          host:(NSString *)host
                          path:(NSString *)path
               queryParameters:(NSDictionary *)queryParameters NS_DESIGNATED_INITIALIZER;

- (instancetype)init __attribute__((unavailable("'init' not available, use 'initWithScheme:host:path:queryParameters:' instead.")));

/**
 Starts search.

 @param string Search string.
 @param completion Search completion block.
 */
- (void)searchWithString:(NSString *)string
              completion:(GDDataProviderCompletionBlock)completion;

/**
 Cancels search.
 */
- (void)cancelSearch;

@end
