//
//  UIImage+GDImages.h
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (GDImages)

+ (UIImage *)gd_artwork_placeholder;

@end
