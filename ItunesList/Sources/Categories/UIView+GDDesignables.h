//
//  UIView+GDDesignables.h
//  ItunesList
//
//  Created by Daniil on 26.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (GDDesignables)

@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable UIColor *borderColor;

- (void)configureDesignableView;

@end
