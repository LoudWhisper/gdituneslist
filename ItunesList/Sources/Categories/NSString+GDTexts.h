//
//  NSString+GDTexts.h
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (GDTexts)

+ (NSString *)gd_title_list_viewcontroller;
+ (NSString *)gd_title_detail_list_viewcontroller;

+ (NSString *)gd_title_error_alert;

+ (NSString *)gd_title_alert_button_close;

+ (NSString *)gd_error_message_request_timedout;
+ (NSString *)gd_error_message_request_failed;
+ (NSString *)gd_error_message_invalid_link;

@end
