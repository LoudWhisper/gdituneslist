//
//  UIView+GDDesignables.m
//  ItunesList
//
//  Created by Daniil on 26.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "UIView+GDDesignables.h"

#import <objc/runtime.h>

@implementation UIView (GDDesignables)

static NSString *const gd_corner_radius_key = @"gd.categoryProperties.runtimeKey.cornerRadius";
static NSString *const gd_border_width_key  = @"gd.categoryProperties.runtimeKey.borderWidth";
static NSString *const gd_border_color_key  = @"gd.categoryProperties.runtimeKey.borderColor";

#pragma mark -
#pragma mark - Init Methods & Superclass Overriders

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self configureDesignableView];
}

- (void)prepareForInterfaceBuilder {
    [super prepareForInterfaceBuilder];
    
    [self configureDesignableView];
}

#pragma mark -
#pragma mark - Public Methods

- (void)configureDesignableView {
    [self.layer setCornerRadius:self.cornerRadius];
    [self.layer setBorderWidth:self.borderWidth];
    [self.layer setBorderColor:[self.borderColor CGColor]];
    
    [self.layer setMasksToBounds:YES];
}

#pragma mark -
#pragma mark - Setters & Getters

- (void)setCornerRadius:(CGFloat)cornerRadius {
    objc_setAssociatedObject(self, &gd_corner_radius_key, @(cornerRadius), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self.layer setCornerRadius:cornerRadius];
}

- (CGFloat)cornerRadius {
    id cornerRadius = objc_getAssociatedObject(self, &gd_corner_radius_key);
    if (cornerRadius && [cornerRadius isKindOfClass:[NSNumber class]]) {
        return [cornerRadius doubleValue];
    }
    return 0.0;
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    objc_setAssociatedObject(self, &gd_border_width_key, @(borderWidth), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self.layer setBorderWidth:borderWidth];
}

- (CGFloat)borderWidth {
    id borderWidth = objc_getAssociatedObject(self, &gd_border_width_key);
    if (borderWidth && [borderWidth isKindOfClass:[NSNumber class]]) {
        return [borderWidth doubleValue];
    }
    return 0.0;
}

- (void)setBorderColor:(UIColor *)borderColor {
    objc_setAssociatedObject(self, &gd_border_color_key, borderColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self.layer setBorderColor:[borderColor CGColor]];
}

- (UIColor *)borderColor {
    id borderColor = objc_getAssociatedObject(self, &gd_border_color_key);
    if (borderColor && [borderColor isKindOfClass:[UIColor class]]) {
        return borderColor;
    }
    return nil;
}

@end
