//
//  GDRouter+PropertiesContainer.h
//  ItunesList
//
//  Created by Daniil Gavrilov on 13.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "GDRouter.h"

@class UINavigationController;
@class GDViewControllerFabric;

@interface GDRouter (PropertiesContainer)

@property (nonatomic, strong) UINavigationController *rootNavigationController;
@property (nonatomic, strong) GDRouter *childRouter;
@property (nonatomic, weak) GDRouter *parentRouter;
@property (nonatomic, strong) GDViewControllerFabric *viewControllerFabric;

@end
