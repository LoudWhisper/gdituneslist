//
//  UIColor+GDColors.h
//  ItunesList
//
//  Created by Daniil on 26.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (GDColors)

+ (UIColor *)gd_light_color;
+ (UIColor *)gd_dark_color;

@end
