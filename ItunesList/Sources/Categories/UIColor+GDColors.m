//
//  UIColor+GDColors.m
//  ItunesList
//
//  Created by Daniil on 26.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "UIColor+GDColors.h"

@implementation UIColor (GDColors)

+ (UIColor *)gd_light_color {
    return [UIColor colorWithRed:(245.0 / 255.0) green:(245.0 / 255.0) blue:(245.0 / 255.0) alpha:1.0];
}

+ (UIColor *)gd_dark_color {
    return [UIColor colorWithRed:(62.0 / 255.0) green:(65.0 / 255.0) blue:(81.0 / 255.0) alpha:1.0];
}

@end
