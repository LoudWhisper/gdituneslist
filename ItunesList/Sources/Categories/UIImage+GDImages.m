//
//  UIImage+GDImages.m
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "UIImage+GDImages.h"

@implementation UIImage (GDImages)

+ (UIImage *)gd_artwork_placeholder {
    return [UIImage imageNamed:@"gd_artwork_placeholder"];
}

@end
