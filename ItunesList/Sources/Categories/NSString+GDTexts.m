//
//  NSString+GDTexts.m
//  ItunesList
//
//  Created by Daniil on 14.02.2018.
//  Copyright © 2018 GD. All rights reserved.
//

#import "NSString+GDTexts.h"

@implementation NSString (GDTexts)

#pragma mark - View Controller Titles

+ (NSString *)gd_title_list_viewcontroller {
    return @"iTunes List";
}

+ (NSString *)gd_title_detail_list_viewcontroller {
    return @"iTunes Item";
}

#pragma mark - Alert Titles

+ (NSString *)gd_title_error_alert {
    return @"An error occurred";
}

#pragma mark - Alert Button Titles

+ (NSString *)gd_title_alert_button_close {
    return @"Ok";
}

#pragma mark - Alert Messages

+ (NSString *)gd_error_message_request_timedout {
    return @"Request timed out. Please check your internet connection and try again.";
}

+ (NSString *)gd_error_message_request_failed {
    return @"Request failed. Please contact with support team and try again later.";
}

+ (NSString *)gd_error_message_invalid_link {
    return @"Link is invalid.";
}

@end
