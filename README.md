# iTunes List

This example follows MVVM architecture. You can see how we can configure the same view controller to show different information. For this example we are using [iTunes API](https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/).


## Structure

* Routers
* View Controllers
* Views
* Models
* Services
* Helpers
* Protocols
* Categories
* Other
* Tests

## Routers

Routers shows us and configures view controllers. You can create any other classes to replace the configuration of the view controller to follow the principle of single responsibility.

All routers have a strong references to their navigation controller, a child router and a weak reference to the parent router. The child router is stored as a weak reference to avoid retain cycle.

```objective-c
@interface GDRouter ()

@property (nonatomic, strong) UINavigationController *rootNavigationController;
@property (nonatomic, strong) GDRouter *childRouter;
@property (nonatomic, weak) GDRouter *parentRouter;

@end
```

The root router is created and stored as a strong reference in AppDelegate.

```objective-c
- (void)setupLaunchScreen {
    CGRect frame = [[UIScreen mainScreen] bounds];
    UIWindow *window = [[UIWindow alloc] initWithFrame:frame];
    GDLaunchRouter *launchRouter = [[GDLaunchRouter alloc] initWithWindow:window];
    
    self.window = window;
    self.launchRouter = launchRouter;
}
```

It configures the navigation stack and sets it as the root view controller for the passed window.

```objective-c
- (UINavigationController *)setupLaunchNavigationControllerWithWindow:(UIWindow *)window {
    NSString *viewControllerIdentifier = NSStringFromClass([GDLaunchViewController class]);
    GDStoryboardType storyboardType = GDStoryboardTypeLaunch;
    
    GDLaunchViewController *launchViewController = [self.viewControllerFabric createViewControllerWithIdentifier:viewControllerIdentifier fromStoryboardWithType:storyboardType];
    GDLaunchViewModel *launchViewModel = [[GDLaunchViewModel alloc] initWithOutput:launchViewController];
    
    [launchViewController setRouter:self];
    [launchViewController setViewModel:launchViewModel];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:launchViewController];
    [window setRootViewController:navigationController];
    return navigationController;
}
```

## View Controllers

All view controllers have a weak reference to their router, a strong reference to their view model, and correspond to the view model output protocol. The router is stored as a weak reference to avoid retain cycle. Both router and view model is closed by the protocol.

```objective-c
@interface GDLaunchViewController : UIViewController <GDLaunchViewModelOutput>

@property (nonatomic, weak) id<GDLaunchScreensProvider> router;
@property (nonatomic, strong) id<GDLaunchViewModelInput> viewModel;

@end
```

The view controller notifies its view model about the life cycle and user actions.

```objective-c
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.viewModel viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.viewModel viewWillAppear];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.viewModel inputTextChanged:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar setText:nil];
    [searchBar resignFirstResponder];
    
    [self.viewModel cancelSearchButtonClicked];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    
    [self.viewModel startSearchButtonClicked];
}
```

he view model also notifies the view controller of the changes that should be displayed. The delegate is used for notification, but for this you can use any other mechanism.

```objective-c
- (void)listViewModelSearchResultsChanged:(NSArray<GDListModel *> *)results {
    [self.tableViewModel setListModels:results];
    
    if ([self isViewLoaded] && [self.view window]) {
        [self.tableView reloadData];
        [self applySearchCompletedState];
    }
}

- (void)listViewModelDidReceiveErrorWithTitle:(NSString *)title message:(NSString *)message {
    NSString *closeActionTitle = [NSString gd_title_alert_button_close];
    UIAlertAction *closeAction = [UIAlertAction actionWithTitle:closeActionTitle style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:closeAction];
    
    if ([self isViewLoaded] && [self.view window]) {
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)listViewModelSearchStateChanged:(GDListViewModelSearchState)state {
    switch (state) {
        case GDListViewModelSearchStateActive:
            [self applySearchActiveState];
            break;
        case GDListViewModelSearchStateInactive:
            [self applySearchInactiveState];
            break;
        case GDListViewModelSearchStateCancelled:
            [self applySearchCancelledState];
            break;
    }
}
```

## Views

### Storyboards

Each storyboard represents its own user story. For example, launch, authorization, conversations, profile and so on. Don't make the storyboards large, separate them. Ideally, each router must have its own storyboard.

### Custom Views

Reusable controls, designable views, cells and so on.

### Xibs

Representation of the custom views.

## Models

### View Models

One view models are used in view controllers, injects from routers and closes with the protocols. Interacts with the data models. Processes the life cycle of the view controller and the user's actions.

Other view models are used to assist with large delegates, such as the table delegate and the data source. It is created as an object in the storyboard and stored as a weak IBOutlet reference in the view controller.

### Data Models

Used in the view models and works with the services. Interacts with servers, databases and third-party services.

### Object Models

Data structures, stores information. 

## Services

Services provide access to the server API, access to the database or any other ready-to-use information, for example, to network addresses. They are used in routers and data models.

## Helpers

Helpers are used in services or other helpers and perform small and narrow-purpose operations. For example, prepare an exception, run a network request, prepare a request endpoint.

## Protocols

Protocols. It most cases they are used to close view models and routers.

## Categories

Categories. UIImage, NSString and UIColor categories are used to provide access to images, strings and colors for the entire application. All images, strings and colors should be obtained from here, except for default colors, such as white or clear, and constant strings, such as keys or paths.

## Other

All other things. For example, string constants.

## Tests

Take a look at the tests. We can test most of our services, helpers and data providers.
